export default {
    listCards: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        padding: '20px',
        backgroundColor: 'rgba(194, 201, 206, 1)',
        border: '1px double',
        borderColor: 'rgb(255, 255, 255)',
        borderRadius: '4px'
    },

    formContainer: {
        margin: "10px 0",
        padding: '20px',
        backgroundColor: 'rgba(194, 201, 206, 1)',
        border: '1px double',
        borderColor: 'rgb(255, 255, 255)',
        borderRadius: '4px'
    },
}