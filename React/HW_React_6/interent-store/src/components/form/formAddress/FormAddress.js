import React, {useState} from 'react';
import injectSheet from "react-jss";
import styles from "./FormAddressStyles";
import PropTypes from "prop-types";
import Button from "../../button/Button";
import * as formAddressActions from "../../../actions/formAddressActions";
import {useDispatch, useSelector} from "react-redux";
import InputFrom from "../input/InputForm";

function FormAddress({classes, handleSubmit}) {

    const formList = useSelector(state => state.formAddressReducer);
    const dispatch = useDispatch();

    const [error, setError] = useState({
        field: '',
        message: ''
    });

    const onChange = (event) => {
        dispatch(formAddressActions.addUserInfo(event.target.name, event.target.value));
    };

    const submitForm = (event) => {
        event.preventDefault();

        if (!formList.userName) {
            setError({
                ...error,
                ...{
                    field: 'userName',
                    message: 'This field should be filled'
                }
            });
            return false;

        } else if (!formList.userSurname) {
            setError({
                ...error,
                ...{
                    field: 'userSurname',
                    message: 'This field should be filled'
                }
            });
            return false;

        } else if (!formList.userAge || isNaN(Number(formList.userAge))) {
            setError({
                ...error,
                ...{
                    field: 'userAge',
                    message: 'This field should contain number'
                }
            });
            return false;

        } else if (!formList.userAddress) {
            setError({
                ...error,
                ...{
                    field: 'userAddress',
                    message: 'This field should be filled'
                }
            });
            return false;

        } else if (!formList.userPhone || isNaN(Number(formList.userAge))) {
            setError({
                ...error,
                ...{
                    field: 'userPhone',
                    message: 'This field should contain number'
                }
            });
            return false;

        } else {
            handleSubmit();
        }
    };

    return (
        <form className={classes.formAddress} name="Info user" onSubmit={submitForm}>
            <label className={classes.formAddressInputTitle} htmlFor="userName">
                <span>Имя пользователя</span>
                <InputFrom name="userName"
                           placeholder="Enter your name"
                           type="text"
                           value={formList.userName}
                           onChange={onChange}/>
                {
                    error.field === 'userName'
                        ? (<div className={classes.formAddressInputError}>
                            {error.message}
                        </div>)
                        : ''
                }
            </label>

            <label className={classes.formAddressInputTitle} htmlFor="userSurname">
                <span>Фамилия пользователя</span>
                <InputFrom name="userSurname"
                           placeholder="Enter your surname"
                           type="text"
                           value={formList.userSurname}
                           onChange={onChange}/>
                {
                    error.field === 'userSurname'
                        ? (<div className={classes.formAddressInputError}>
                            {error.message}
                        </div>)
                        : ''
                }
            </label>

            <label className={classes.formAddressInputTitle} htmlFor="userAge">
                <span>Возраст пользователя</span>
                <InputFrom name="userAge"
                           placeholder="Enter your age"
                           type="number"
                           value={formList.userAge}
                           onChange={onChange}/>
                {
                    error.field === 'userAge'
                        ? (<div className={classes.formAddressInputError}>
                            {error.message}
                        </div>)
                        : ''
                }
            </label>

            <label className={classes.formAddressInputTitle} htmlFor="userAddress">
                <span>Адрес доставки</span>
                <InputFrom name="userAddress"
                           placeholder="Enter your address"
                           type="text"
                           value={formList.userAddress}
                           onChange={onChange}/>
                {
                    error.field === 'userAddress'
                        ? (<div className={classes.formAddressInputError}>
                            {error.message}
                        </div>)
                        : ''
                }
            </label>

            <label className={classes.formAddressInputTitle} htmlFor="userPhone">
                <span>Мобильный телефон</span>
                <InputFrom name="userPhone"
                           placeholder="Enter your mobile phone number"
                           type="tel"
                           value={formList.userPhone}
                           onChange={onChange}/>
                {
                    error.field === 'userPhone'
                        ? (<div className={classes.formAddressInputError}>
                            {error.message}
                        </div>)
                        : ''
                }
            </label>

            <Button additionClass="btnBuy" type="submit" text="Checkout"/>
        </form>
    );
}

FormAddress.propTypes = {
    additionClass: PropTypes.string,
    classes: PropTypes.object,
    handleSubmit: PropTypes.func
};

export default injectSheet(styles)(FormAddress);