export default {

    formAddress: {
        display: "flex",
        flexDirection: "column",
        maxWidth: "500px",
        padding: "20px",
        backgroundColor: "rgba(194, 201, 206, 1)",
    },

    formAddressInputTitle: {
        position: "relative",
        display: "flex",
        flexDirection: "column",
        fontSize: "18px"
    },

    formAddressInputError: {
        position: "absolute",
        bottom: 0,
        left: 0,
        color: "rgb(230, 70, 60)"
    }

}