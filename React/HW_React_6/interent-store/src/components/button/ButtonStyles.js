export default {

    btn: {
        fontSize: '14px',
        border: 'none',
        borderRadius: '5px',
        cursor: 'pointer',
    },

    btnAddToCard: {
        padding: '8px',
        color: 'rgb(255, 255, 255)',
        backgroundColor: 'rgb(0, 0, 0)',

        '&:hover': {
            color: 'rgb(0, 0, 0)',
            backgroundColor: 'rgba(48, 164, 91)',
        }
    },

    btnAddToFavoriteFalse: {
        padding: 0,
        color: 'rgb(255, 255, 255)',
        backgroundColor: 'rgba(48, 66, 75, 0.55)',

        '&:hover': {
            color: 'rgb(0, 0, 0)',
        }
    },

    btnAddToFavoriteTrue: {
        padding: 0,
        color: 'rgb(0, 0, 0)',
        backgroundColor: 'rgba(48, 66, 75, 0.55)',

        '&:hover': {
            color: 'rgb(255, 255, 255)',
        }
    },

    btnOk: {
        padding: '8px',
    },

    btnCancel: {
        padding: '8px',
    },

    btnRemove: {
        padding: '8px',
        color: 'rgb(255, 255, 255)',
        backgroundColor: 'rgba(255, 0, 0, 0.8)',

        '&:hover': {
            color: 'rgb(0, 0, 0)',
            backgroundColor: 'rgb(255, 0, 0)',
        }
    },

    btnBuy: {
        padding: '8px',
        color: 'rgb(255, 255, 255)',
        backgroundColor: 'rgba(20, 150, 50, 0.8)',
        border: "1px solid rgb(0, 0, 0)",

        '&:hover': {
            color: 'rgb(0, 0, 0)',
            backgroundColor: 'rgb(20, 150, 50)',
        }
    }
}