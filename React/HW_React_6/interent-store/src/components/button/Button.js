import React from 'react';
import injectSheet from "react-jss";
import styles from "./ButtonStyles";
import PropTypes from "prop-types";

function Button(props) {

    const {backgroundColor, text, onClick, classes, additionClass, type} = props;

    const findAdditionalClass = () => {
        for (let prop in classes) {
            if(prop === additionClass) {
                return classes[prop];
            }
        }
        return '';
    };

    return (
        <button
            className={`${classes.btn} ${findAdditionalClass()}`}
            style={{backgroundColor: backgroundColor}}
            type={type}
            onClick={onClick}>
            {text}
        </button>
    );
}

Button.propTypes = {
    additionClass: PropTypes.string,
    classes: PropTypes.object,
    backgroundColor: PropTypes.string,
    type: PropTypes.string,
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    onClick: PropTypes.func
};

export default injectSheet(styles)(Button);