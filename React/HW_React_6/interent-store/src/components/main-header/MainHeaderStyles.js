export default {

    header: {
        position: 'relative',
    },

    headerList: {
        display: 'flex',
        justifyContent: 'space-between',
        margin: '20px 0',
        padding: '0 50px',
        listStyle: 'none',
    },

    headerListItem: {
        marginRight: '10px',
        backgroundColor: 'rgba(194, 201, 206, 1)',
        border: '1px solid black',
        borderRadius: '4px',
        cursor: 'pointer',

        '&:last-child': {
            marginRight: 0,
        },

        '&:hover': {
            backgroundColor: 'rgba(48, 66, 75)',

            '& > a': {
                color: 'rgb(30, 200, 230)'
            }
        },

        '& > a': {
            display: 'block',
            padding: '10px',
            color: 'rgb(255, 255, 255)',
            textDecoration: 'none',
        }
    }
}