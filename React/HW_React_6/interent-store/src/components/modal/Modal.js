import React, {useEffect, useRef} from 'react';
import Button from "../button/Button";
import injectSheet from "react-jss";
import styles from "./ModalStyles";
import PropTypes from 'prop-types';
import isNil from "lodash/fp/isNil";

function Modal(props) {

    const {header, closeButton, text, actions, classes, onClose} = props;
    const modal = useRef(null);

    useEffect( () => {
            document.addEventListener("click", handlerCloseModal, false);

            return () => {
                document.addEventListener("click", handlerCloseModal, false);
            }
    });

    const handlerCloseModal = (e) => {
        if (!isNil(modal.current)) {
            if (!modal.current.contains(e.target)) {
                onClose();
            }
        }
    };

    return (
        <div className={classes.modal}>
            <div className={classes.modalContainer} ref={modal}>
                <div className={classes.modalHeader}>
                    <h2 className={classes.modalHeaderTitle}>
                        {header}
                    </h2>
                    {closeButton &&
                    <div className={classes.modalBtnClose}>
                        <Button text="X" onClick={onClose}/>
                    </div>
                    }
                </div>
                <div className={classes.modalBody}>
                    <p className={classes.modalBodyText}>
                        {text}
                    </p>
                </div>
                {actions.length !== 0 &&
                <div className={classes.modalFooter}>
                    {actions}
                </div>
                }
            </div>
        </div>
    );
}

Modal.propTypes = {
    classes: PropTypes.object,
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.array,
    onClose: PropTypes.func,
};

export default injectSheet(styles)(Modal);