import React from 'react';
import {Switch, Route} from "react-router-dom";
import HomeListCards from "../components/Home/home-list-cards/HomeListCards";
import FavoritesListCards from "../components/Favorites/favorites-list/FavoritesListCards";
import CartListCards from "../components/Cart/cart-list/CartListCards";
import {routes} from '../constants/constRouters';

export default function Layout() {

    return (
        <Switch>
            <Route exact path={routes.home.href} component={() => <HomeListCards/>}/>
            <Route exact path={routes.cart.href} component={() => <CartListCards/>}/>
            <Route exact path={routes.favorites.href} component={() => <FavoritesListCards/>}/>
        </Switch>
    )
}