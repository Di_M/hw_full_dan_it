import { combineReducers } from 'redux'
import homeListReducer from './HomeListReducer';
import favoriteListReducer from './FavoriteListReducer';
import cartListReducer from './CartListReducer';
import modalReducer from './ModalReducer';
import formAddressReducer from './FormAddressReducer';


export default combineReducers({
    homeListReducer,
    favoriteListReducer,
    cartListReducer,
    modalReducer,
    formAddressReducer
})