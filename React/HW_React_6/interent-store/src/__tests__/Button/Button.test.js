import React from 'react';
import {render, shallow} from 'enzyme';
import sinon from 'sinon';
import Button from '../../components/button/Button';


describe('Button component', () => {

    const props = {
        text: '',
        backgroundColor: '',
        type: '',
        onClick: () => {}
    };

    describe('Button text props', () => {

        it('should render button with text Add to Cart', () => {

            const nextProps = {
                ...props,
                text: "Add to Card"
            };

            const wrapper = render(<Button {...nextProps}/>);
            expect(wrapper.text()).toEqual('Add to Card');
        });

        it('should render button with Favorite Star', () => {

            const iconFavorite = <i className="fas fa-star"/>;
            const nextProps = {
                ...props,
                text: iconFavorite
            };

            const wrapper = render(<Button {...nextProps}/>);
            expect(wrapper.find('i').hasClass('fas')).toEqual(true);
        });


        it('should render button with backgroundColor = red', () => {

            const nextProps = {
                ...props,
                backgroundColor: "red"
            };

            const wrapper = render(<Button {...nextProps}/>);
            expect(wrapper.get('0').attribs.style).toEqual('background-color:red');
        });
    });


    describe('Button text props', () => {

        it('should render button with type = "button"', () => {

            const nextProps = {
                ...props,
                type: "button"
            };

            const wrapper = render(<Button {...nextProps}/>);
            expect(wrapper.get('0').attribs.type).toEqual('button');
        });
    });

    describe('Button click', () => {

        it('simulates click events', () => {
            const onButtonClick = jest.fn();
            const nextProps = {
                ...props,
                text: "Ok",
                type: 'button',
                onClick: onButtonClick
            };

            const wrapper = mount(<Button {...nextProps}/>);
            wrapper.props().onClick();
            expect(onButtonClick).toHaveBeenCalled();
        });
    });
});