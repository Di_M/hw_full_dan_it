import React, { useEffect } from 'react';
import { useDispatch } from "react-redux";
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./AppStyles";
import MainHeader from '../../components/main-header/MainHeader';
import Layout from "../../routers/Routers";
import * as homeListActions from '../../actions/homeListActions';
import * as favoriteListActions from '../../actions/favoriteListActions';
import * as cartListActions from '../../actions/cartListActions';
import LocalStorage from "../../services/LocalStorage";


function App({classes}) {

    const dispatch = useDispatch();

    useEffect(() => {
        fetch('headphones.json')
            .then(resp => resp.json())
            .then(result => {
                console.log(result);
                dispatch(homeListActions.getList(result))
            });
    });

    useEffect(() => {
        const array = LocalStorage.getCardsFromLocalStorage('favorite');

        if (array) {
            dispatch(favoriteListActions.getList(array));
        }
    });

    useEffect(() => {
        const array = LocalStorage.getCardsFromLocalStorage('cart');

        if (array) {
            dispatch(cartListActions.getList(array));
        }
    });

    return (
        <div className={classes.app}>
            <MainHeader />
            <Layout />
        </div>
    )
}

App.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(App);
