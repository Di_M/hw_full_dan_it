import * as types from '../constants/constTypes';

export function addUserInfo(title, info) {

    return dispatch => {
        dispatch({
            type: types.ADD_USER_INFO,
            title: title,
            payload: info
        });
    };
}

export function clearInfo() {

    return dispatch => {
        dispatch({
            type: types.CLEAR_USER_INFO,
        });
    };
}