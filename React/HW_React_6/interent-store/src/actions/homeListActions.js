import * as types from '../constants/constTypes';

export function getList(list) {

    return dispatch => {
        dispatch({
            type: types.ADD_HOME_LIST,
            payload: list
        });
    };
}