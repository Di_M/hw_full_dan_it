export default {

    modal: {
        position: 'absolute',
        top: 0,
        left: 0,
        display: 'flex',
        width: '100vw',
        height: '100vh',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
    },

    modalContainer: {
        margin: 'auto',
        backgroundColor: 'rgba(255, 0, 0, 0.8)',
        borderRadius: '5px',
    },

    modalHeader: {
        position: 'relative',
        width: '100%',
        textAlign: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
    },

    modalHeaderTitle: {
        margin: 0,
        padding: '10px',
        fontSize: '16px',
    },

    modalBtnClose: {
        position: 'absolute',
        top: 0,
        right: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        borderRadius: 0,
        cursor: 'pointer',
    },

    modalBody: {
        padding: '0 50px',
    },

    modalBodyText: {
        margin: '30px 0',
        fontSize: '16px',
    },

    modalFooter: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: '20px 0',
    }
}