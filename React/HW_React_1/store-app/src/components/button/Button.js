import React, {Component} from 'react';
import injectSheet from "react-jss";
import styles from "./ButtonStyles";
import PropTypes from "prop-types";

class Button extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        const {backgroundColor, text, onClick, classes} = this.props;

        return (
            <button
                className={classes.btn}
                style={{backgroundColor: backgroundColor}}
                type="button"
                onClick={onClick}>
                {text}
            </button>
        );
    }
}

Button.propTypes = {
    classes: PropTypes.object,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func
};

export default injectSheet(styles)(Button);