export default {

    btn: {
        padding: '8px',
        fontSize: '14px',
        backgroundColor: 'none',
        border: 'none',
        borderRadius: '5px',
        cursor: 'pointer',
    }
}