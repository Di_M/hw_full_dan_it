export default {

    '@global': {
        'body': {
            margin: 0,
            padding: 0,
        },
    },

    app: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
    }
}