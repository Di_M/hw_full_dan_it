import React, {Component} from 'react';
import injectSheet from "react-jss";
import styles from "./AppStyles";
import Button from '../components/button/Button'
import Modal from "../components/modal/Modal";
import PropTypes from 'prop-types';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showFirstModal: false,
      showSecondModal: false,
    };
    this.handleToggleFirstModal = this.handleToggleFirstModal.bind(this);
    this.handleToggleSecondModal = this.handleToggleSecondModal.bind(this);
  }

  handleToggleFirstModal() {
    this.setState({showFirstModal: !this.state.showFirstModal});
  }

  handleToggleSecondModal() {
    this.setState({showSecondModal: !this.state.showSecondModal});
  }

  render() {

    const {classes} = this.props;
    const {showFirstModal, showSecondModal} = this.state;
    const firstModalButtons = [
        <Button key="1" text="Ok" backgroundColor="grey" onClick={this.handleToggleFirstModal}/>,
        <Button key="2" text="Cancel" backgroundColor="grey" onClick={this.handleToggleFirstModal}/>
    ];
    const secondModalButtons = [
        <Button key="1" text="Ok" backgroundColor="yellow" onClick={this.handleToggleSecondModal}/>,
        <Button key="2" text="Cancel" backgroundColor="yellow" onClick={this.handleToggleSecondModal}/>
    ];

    return (
        <div className={classes.app}>
          <Button text="Open first modal" backgroundColor="red" onClick={this.handleToggleFirstModal}/>
          {showFirstModal && <Modal
              header="Do you want to delete this file?"
              closeButton="true"
              text="Once you delete this file, it won’t be possible to undo this action.
                    Are you sure you want to delete it?"
              actions={firstModalButtons}
              onClose={this.handleToggleFirstModal}/>}
          <Button text="Open second modal" backgroundColor="blue" onClick={this.handleToggleSecondModal}/>
          {showSecondModal && <Modal
              header="Hi everyone!"
              closeButton="false"
              text="It another modal window. You can do accept or cancel it. Good luck!"
              actions={secondModalButtons}
              onClose={this.handleToggleSecondModal}/>}
        </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object
};

export default injectSheet(styles)(App);
