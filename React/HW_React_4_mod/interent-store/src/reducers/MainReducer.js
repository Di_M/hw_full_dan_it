import { combineReducers } from 'redux'
import homeListReducer from './HomeListReducer';
import favoriteListReducer from './FavoriteListReducer';
import cartListReducer from './CartListReducer';
import modalReducer from './ModalReducer';

export default combineReducers({
    homeListReducer,
    favoriteListReducer,
    cartListReducer,
    modalReducer
})