import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./ListCardsStyles";
import Card from '../card/Card';
import Button from '../button/Button';
import Modal from "../modal/Modal";
import * as modalActions from "../../actions/modalActions";
import LocalStorage from "../../services/LocalStorage";
import * as cartListActions from "../../actions/cartListActions";
import * as favoriteListActions from "../../actions/favoriteListActions";


function ListCard(props) {

    const {classes, list} = props;
    const isOpenModal = useSelector(state => state.modalReducer);
    const favoriteList = useSelector(state => state.favoriteListReducer.favoriteList);
    const cartList = useSelector(state => state.cartListReducer.cartList);
    const dispatch = useDispatch();
    let [itemCart, setItemCart] = useState(null);

    const handleToggleModal = () => {
        dispatch(modalActions.showModal());
    };

    const getItem = (data) => {
        setItemCart(data);
    };

    const checkItemInCart = (data) => {
        let elem = cartList.find(element => {return element.article === data.article});
        return !!elem;
    };

    const checkItemInFavorite = (data) => {
        let elem = favoriteList.find(element => {return element.article === data.article});
        return !!elem;
    };

    const handleClickToCart = () => {
        LocalStorage.postCardToLocalStorage('cart', itemCart);
        dispatch(cartListActions.addItem(itemCart));
        handleToggleModal();
    };

    const handleDeleteFromCart = () => {
        LocalStorage.deleteCardFromLocalStorage('cart', itemCart.article);
        let newList = cartList.filter(elem => elem.article !== itemCart.article);
        dispatch(cartListActions.updateList([...newList]));
        handleToggleModal();
    };

    const handleClickToFavorite = (data) => {
        if(!checkItemInFavorite(data)) {
            LocalStorage.postCardToLocalStorage('favorite', data);
            dispatch(favoriteListActions.addItem(data));
        } else {
            LocalStorage.deleteCardFromLocalStorage('favorite', data.article);
            let newList = favoriteList.filter(elem => elem.article !== data.article);
            dispatch(favoriteListActions.updateList([...newList]));
        }
    };

    const buttonsDefaultModal = [
        <Button key="1" additionClass="btnOk" text="Ok" onClick={handleClickToCart}/>,
        <Button key="2" additionClass="btnCancel" text="Cancel" onClick={handleToggleModal}/>
    ];
    const buttonsDeleteModal = [
        <Button key="1" additionClass="btnOk" text="Ok" onClick={handleDeleteFromCart}/>,
        <Button key="2" additionClass="btnCancel" text="Cancel" onClick={handleToggleModal}/>
    ];

    const cardObject = list.map((item, i) => {

        return <Card key={i}
                     data={item}
                     inFavorite={checkItemInFavorite(item)}
                     inCart={checkItemInCart(item)}
                     handlerClickCart={handleToggleModal}
                     getData={getItem}
                     handelFavorite={handleClickToFavorite}/>
    });

    return (
        <div className={classes.listCards}>
            {cardObject}

            {isOpenModal && checkItemInCart(itemCart) && <Modal
                header="Are yor sure?"
                closeButton={true}
                text="Do you want to remove this product from the cart?"
                actions={buttonsDeleteModal}
                onClose={handleToggleModal}/>}
            {isOpenModal && !checkItemInCart(itemCart) && <Modal
                header="Are yor sure?"
                closeButton={true}
                text="Do you want to add this product to the cart?"
                actions={buttonsDefaultModal}
                onClose={handleToggleModal}/>}
        </div>
    );
}

ListCard.propTypes = {
    classes: PropTypes.object,
    list: PropTypes.object
};

export default injectSheet(styles)(ListCard);