import React from 'react';
import PropTypes from 'prop-types';
import ListCards from "../Home/HomeListCards";


function FavoritesListCards({list}) {

    return (
        <div >
            <ListCards list = {list}/>
        </div>
    );
}

FavoritesListCards.propTypes = {
    list: PropTypes.object
};

export default (FavoritesListCards);