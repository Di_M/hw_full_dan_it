import React from 'react';
import PropTypes from 'prop-types';
import ListCards from "../Home/HomeListCards";

function CartListCards({list}) {

    return (
        <div>
            <ListCards list = {list}/>
        </div>
    );
}

CartListCards.propTypes = {
    list: PropTypes.object
};

export default (CartListCards);