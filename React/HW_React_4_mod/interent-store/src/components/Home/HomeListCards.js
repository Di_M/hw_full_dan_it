import React from 'react';
import PropTypes from 'prop-types';
import ListCards from "../listCards/ListCards";


function HomeListCards({list}) {

    return (
        <div>
            <ListCards list = {list}/>
        </div>
    );
}

HomeListCards.propTypes = {
    list: PropTypes.object
};

export default (HomeListCards);