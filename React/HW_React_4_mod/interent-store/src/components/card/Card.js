import React from 'react';
import injectSheet from "react-jss";
import styles from "./CardStyle";
import PropTypes from 'prop-types';
import Button from '../button/Button';


function Card(props) {

    const {data, classes, handlerClickCart, inFavorite, inCart, handelFavorite, getData} = props;

    const handlerCart = () => {
        handlerClickCart();
        getData(data);
    };

    const handleClickToFavorite = () => {
        handelFavorite(data);
    };

    const iconFavorite = <i className="fas fa-star"/>;

    return(

        <div className={classes.card}>
            <img className={classes.cardImage} src={data.url} alt={data.name} width="100"/>
            <h3 className={classes.cardTitle}>
                {data.name}
            </h3>
            <p className={classes.cardArticle}>
                Article: {data.article}
            </p>
            <div className={classes.cardPrice}>
                {data.price} $
            </div>
            <div className={classes.cardButtonsContainer}>
                <Button additionClass={inFavorite ? "btnAddToFavoriteTrue" : "btnAddToFavoriteFalse"}
                        text={iconFavorite}
                        onClick={handleClickToFavorite}/>
                {inCart ? <Button additionClass="btnRemove" text="Remove from Cart" onClick={handlerCart}/>
                        : <Button additionClass="btnAddToCard" text="Add to Card" onClick={handlerCart}/>}

            </div>
        </div>
    );
}

Card.propTypes = {
    data: PropTypes.object,
    classes: PropTypes.object,
    handlerClickCart: PropTypes.func,
    handelFavorite: PropTypes.func,
    getData: PropTypes.func,
    inFavorite: PropTypes.bool,
    inCart: PropTypes.bool
};

export default injectSheet(styles)(Card);