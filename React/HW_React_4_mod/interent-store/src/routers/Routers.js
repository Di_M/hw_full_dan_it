import React from 'react';
import {Switch, Route} from "react-router-dom";
import HomeListCards from "../components/Home/HomeListCards";
import FavoritesListCards from "../components/Favorites/FavoritesListCards";
import CartListCards from "../components/Cart/CartListCards";
import {routes} from '../constants/constRouters';
import {useSelector} from "react-redux";

export default function Layout() {

    const list = useSelector(state => state.homeListReducer.homeList);
    const favoriteList = useSelector(state => state.favoriteListReducer.favoriteList);
    const cartList = useSelector(state => state.cartListReducer.cartList);

    return (
        <Switch>
            <Route exact path={routes.home.href} component={() => <HomeListCards list = {list}/>}/>
            <Route exact path={routes.cart.href} component={() => <CartListCards list = {cartList}/>}/>}
            <Route exact path={routes.favorites.href} component={() => <FavoritesListCards list = {favoriteList}/>}/>
        </Switch>
    )
}