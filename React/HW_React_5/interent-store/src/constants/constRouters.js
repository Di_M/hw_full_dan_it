export const routes = {
    home: {name: 'Home', href: '/'},
    cart: {name: 'Cart', href: '/cart'},
    favorites: {name: 'Favorites', href: '/favorites'}
};