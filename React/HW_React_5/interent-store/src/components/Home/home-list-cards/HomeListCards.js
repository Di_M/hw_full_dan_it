import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./HomeListCardsStyles";
import Card from '../../card/Card';
import * as modalActions from "../../../actions/modalActions";


function HomeListCards(props) {

    const {classes} = props;
    const list = useSelector(state => state.homeListReducer.homeList);
    const isOpenModal = useSelector(state => state.modalReducer);
    const dispatch = useDispatch();

    const handleToggleModal = () => {
        dispatch(modalActions.showModal());
        return isOpenModal;
    };

    const itemObj = list.map((item, i) => (<Card key={i+item} data={item} toggleModal={handleToggleModal}/>));

    return (
        <div className={classes.listCards}>
            {itemObj}
        </div>
    );
}

HomeListCards.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(HomeListCards);