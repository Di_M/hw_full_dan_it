import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./CartListCardsStyles";
import Card from "../../card/Card";
import * as cartListActions from '../../../actions/cartListActions';
import * as modalActions from "../../../actions/modalActions";
import FormAddress from "../../form/formAddress/FormAddress";
import LocalStorage from "../../../services/LocalStorage";
import * as formAddressActions from "../../../actions/formAddressActions";

function CartListCards(props) {

    const {classes} = props;
    const cartList = useSelector(state => state.cartListReducer.cartList);
    const formList = useSelector(state => state.formAddressReducer);
    const isOpenModal = useSelector(state => state.modalReducer);
    const dispatch = useDispatch();

    const handleToggleModal = () => {
        dispatch(modalActions.showModal());
        return isOpenModal;
    };

    const onDeleteItem = (item) => {
        let newList = cartList.filter(elem => elem.article !== item.article);
        dispatch(cartListActions.updateList([...newList]));
    };

    const onSubmit = () => {
        if (cartList.length !== 0) {
            console.log('Bought goods -->', cartList);
            console.log('Delivery info -->', formList);
            LocalStorage.clearLocalStorage('cart');
            dispatch(formAddressActions.clearInfo());
            dispatch(cartListActions.updateList([]));
        } else {
            alert("Please, add some goods to the cart!");
        }

    };

    const newCartItem = cartList.map((item, i) => (<Card key={item+i} data={item} onDeleteCartCard={onDeleteItem} addedToCart={true} toggleModal={handleToggleModal}/>));

    return (
        <div>
            <div className={classes.listCards}>
                {newCartItem}
            </div>
            <div className={classes.formContainer}>
                <FormAddress handleSubmit={onSubmit}/>
            </div>
        </div>
    );
}

CartListCards.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(CartListCards);