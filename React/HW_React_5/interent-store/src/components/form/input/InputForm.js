import React from 'react';
import styles from "./inputFormStyles";
import PropTypes from "prop-types";
import injectSheet from "react-jss";

function InputFrom (props) {

    const {classes, name, placeholder, type, value, onChange} = props;

    return (
        <>
            <input className={classes.formAddressInput}
                   name={name}
                   placeholder={placeholder}
                   type={type}
                   value={value}
                   onChange={onChange}/>
        </>
    );

}

InputFrom.propTypes = {
    name: PropTypes.string,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string,
    classes: PropTypes.object,
    onChange: PropTypes.func
};

export default injectSheet(styles)(InputFrom);