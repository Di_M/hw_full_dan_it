import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./FavoritesListCardsStyles";
import Card from "../../card/Card";
import * as favoriteListActions from '../../../actions/favoriteListActions';
import * as modalActions from "../../../actions/modalActions";


function FavoritesListCards(props) {

    const {classes} = props;
    const favoriteList = useSelector(state => state.favoriteListReducer.favoriteList);
    const isOpenModal = useSelector(state => state.modalReducer);
    const dispatch = useDispatch();

    const handleToggleModal = () => {
        dispatch(modalActions.showModal());
        return isOpenModal;
    };

    const onDeleteItem = (item) => {
        let newList = favoriteList.filter(elem => elem.article !== item.article);
        dispatch(favoriteListActions.updateList([...newList]));
    };

    const newItems = favoriteList.map((item, i) => (<Card key={i} data={item} onDeleteFavoriteCard={onDeleteItem} addedToCart={false} toggleModal={handleToggleModal}/>));

    return (
        <div className={classes.listCards}>
            {newItems}
        </div>
    );
}

FavoritesListCards.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(FavoritesListCards);