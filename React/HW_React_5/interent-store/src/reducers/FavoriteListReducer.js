import * as types from "../constants/constTypes";

const initState = {
    favoriteList: [],
};

function favoriteListReducer(state = initState, action = {}) {

    switch (action.type) {

        case types.ADD_FAVORITE_LIST:
            return {
                ...state,
                ...{favoriteList: action.payload}
            };

        case types.CHANGE_FAVORITE_LIST:
            return {
                ...{favoriteList: action.payload}
            };

        case types.ADD_FAVORITE_ITEM:
            return {
                ...state,
                favoriteList: [...state.favoriteList, action.payload]
            };

        default:
            return state;
    }
}

export default (favoriteListReducer);