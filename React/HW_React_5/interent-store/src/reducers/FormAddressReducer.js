import * as types from "../constants/constTypes";

const initState = {
    userName: '',
    userSurname: '',
    userAge: '',
    userAddress: '',
    userPhone: ''
};

function formAddressReducer(state = initState, action = {}) {
    switch (action.type) {

        case types.ADD_USER_INFO:
            const cloneState = {...state};
            cloneState[`${action.title}`] = action.payload;
            return cloneState;

        case types.CLEAR_USER_INFO:
            return initState;

        default:
            return state;
    }
}

export default (formAddressReducer);