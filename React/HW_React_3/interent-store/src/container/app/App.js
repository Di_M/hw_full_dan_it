import React from 'react';
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./AppStyles";
import MainHeader from '../../components/main-header/MainHeader';
import Layout from "../../routers/Routers";

function App({classes}) {

    return (
        <div className={classes.app}>
            <MainHeader />
            <Layout />
        </div>
    )
}

App.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(App);
