import React, {useEffect, useState} from 'react';
import injectSheet from "react-jss";
import styles from "./CardStyle";
import PropTypes from 'prop-types';
import Button from '../button/Button';
import Modal from "../modal/Modal";
import LocalStorage from '../../services/LocalStorage';


function Card(props) {

    const {data, classes, onDeleteCard, addedToCart} = props;
    const [showModal, setModal] = useState(false);
    const [favorite, setFavorite] = useState(false);
    const [itemInCart, setItemInCart] = useState(addedToCart);

    useEffect( () => {
        let arrayData = LocalStorage.getCardsFromLocalStorage('favorite');
        if (arrayData) {
            const elem = arrayData.find(element => {return element.article === data.article});
            if (elem) {
                setFavorite(true);
            }
        }
    }, [data.article]);

    useEffect( () => {
        let arrayData = LocalStorage.getCardsFromLocalStorage('cart');
        if (arrayData) {
            const elem = arrayData.find(element => {return element.article === data.article});
            if (elem) {
                setItemInCart(true);
            }
        }
    }, [data.article]);

    const handleClickToCart = () => {
        LocalStorage.postCardToLocalStorage('cart', data);
        setItemInCart(!itemInCart);
        handleToggleModal();
    };

    const handleClickToFavorite = () => {
        if(!favorite) {
            LocalStorage.postCardToLocalStorage('favorite', data);
        } else {
            LocalStorage.deleteCardFromLocalStorage('favorite', data.article);

            if(onDeleteCard) {
                onDeleteCard(data);
            }
        }

        handleToggleFavorite();
    };

    const handleDeleteFromCart = () => {
        LocalStorage.deleteCardFromLocalStorage('cart', data.article);
        setItemInCart(!itemInCart);

        if(onDeleteCard) {
            onDeleteCard(data);
        }
        handleToggleModal();
    };

    const handleToggleFavorite = () => {
        setFavorite(!favorite);
    };

    const handleToggleModal = () => {
        setModal(!showModal);
    };

    const iconFavorite = <i className="fas fa-star"/>;
    const buttonsDefaultModal = [
        <Button key="1" additionClass="btnOk" text="Ok" onClick={handleClickToCart}/>,
        <Button key="2" additionClass="btnCancel" text="Cancel" onClick={handleToggleModal}/>
    ];
    const buttonsDeleteModal = [
        <Button key="1" additionClass="btnOk" text="Ok" onClick={handleDeleteFromCart}/>,
        <Button key="2" additionClass="btnCancel" text="Cancel" onClick={handleToggleModal}/>
    ];

    return(

        <div className={classes.card}>
            <img className={classes.cardImage} src={data.url} alt={data.name} width="100"/>
            <h3 className={classes.cardTitle}>
                {data.name}
            </h3>
            <p className={classes.cardArticle}>
                Article: {data.article}
            </p>
            <div className={classes.cardPrice}>
                {data.price} $
            </div>
            <div className={classes.cardButtonsContainer}>
                <Button additionClass={favorite ? "btnAddToFavoriteTrue" : "btnAddToFavoriteFalse"}
                        text={iconFavorite}
                        onClick={handleClickToFavorite}/>
                {!itemInCart && <Button additionClass="btnAddToCard" text="Add to Card" onClick={handleToggleModal}/>}
                {showModal && !itemInCart && <Modal
                    header="Are yor sure?"
                    closeButton={true}
                    text="Do you want to add this product to the cart?"
                    actions={buttonsDefaultModal}
                    onClose={handleToggleModal}/>}
                {itemInCart && <Button additionClass="btnRemove" text="Remove" onClick={handleToggleModal}/>}
                {showModal && itemInCart && <Modal
                    header="Are yor sure?"
                    closeButton={true}
                    text="Do you want to remove this product from the cart?"
                    actions={buttonsDeleteModal}
                    onClose={handleToggleModal}/>}
            </div>
        </div>
    );
}

Card.propTypes = {
    data: PropTypes.object,
    classes: PropTypes.object,
    onDeleteCard: PropTypes.func,
    addedToCart: PropTypes.bool
};

export default injectSheet(styles)(Card);