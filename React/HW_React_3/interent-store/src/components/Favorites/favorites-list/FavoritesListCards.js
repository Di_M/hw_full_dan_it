import React, {useEffect, useState} from 'react';
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./FavoritesListCardsStyles";
import LocalStorage from "../../../services/LocalStorage";
import Card from "../../card/Card";


function FavoritesListCards({classes}) {

    const [favoriteList, setFavoriteList] = useState([]);

    useEffect( () => {
        const array = LocalStorage.getCardsFromLocalStorage('favorite');

        if (array) {
            setFavoriteList(array);
        }
    }, []);

    const onDeleteItem = (item) => {
        const newList = favoriteList.filter(elem => elem !== item);
        setFavoriteList([...newList]);
    };

    const newItems = favoriteList.map((item, i) => (<Card key={i} data={item} onDeleteCard={onDeleteItem} addedToCart={false}/>));

    return (
        <div className={classes.listCards}>
            {newItems}
        </div>
    );
}

FavoritesListCards.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(FavoritesListCards);