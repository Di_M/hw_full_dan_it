import React, {useEffect, useState} from 'react';
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./CartListCardsStyles";
import LocalStorage from "../../../services/LocalStorage";
import Card from "../../card/Card";


function CartListCards({classes}) {

    const [cartList, setCartList] = useState([]);

    useEffect( () => {
        const array = LocalStorage.getCardsFromLocalStorage('cart');

        if (array) {
            setCartList(array);
        }
    }, []);

    const onDeleteItem = (item) => {
        const newList = cartList.filter(elem => elem !== item);
        setCartList([...newList]);
    };

    const newCartItem = cartList.map((item, i) => (<Card key={item} data={item} onDeleteCard={onDeleteItem} addedToCart={true}/>));

    return (
        <div className={classes.listCards}>
            {newCartItem}
        </div>
    );
}

CartListCards.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(CartListCards);