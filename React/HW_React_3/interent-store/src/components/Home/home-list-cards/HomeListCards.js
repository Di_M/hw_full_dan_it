import React, {useEffect, useState} from 'react';
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./HomeListCardsStyles";
import Card from '../../card/Card';


function HomeListCards({classes}) {

    const [list, setList] = useState([]);

    useEffect( () => {
        fetch('headphones.json')
            .then(resp => resp.json())
            .then(result => setList(listState => {return [...listState, ...result]}));
    }, []);

    const itemObj = list.map((item, i) => (<Card key={i} data={item}/>));

    return (
        <div className={classes.listCards}>
            {itemObj}
        </div>
    );
}

HomeListCards.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(HomeListCards);