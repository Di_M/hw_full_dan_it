import React, {Component} from 'react';
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./ListCardsStyles";
import Card from '../card/Card';


class ListCards extends Component{
    constructor() {
        super();
        this.state = {list: []};
    }

    async componentDidMount() {
        const array = await fetch('headphones.json').then(resp => resp.json());
        console.log('Received data form fetch ->', array);
        this.setState(state => {return {list: [...state.list, ...array]}});
        console.log('New this.state ->', this.state);
    }

    render() {
        const {classes} = this.props;
        const itemObj = this.state.list.map((item, i) => (<Card key={i} data={item}/>));

        return (
            <div className={classes.listCards}>
                {itemObj}
            </div>
        );
    }
}

ListCards.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(ListCards);