import React, {Component} from 'react';
import injectSheet from "react-jss";
import styles from "./CardStyle";
import PropTypes from 'prop-types';
import Button from '../button/Button';
import Modal from "../modal/Modal";
import LocalStorage from '../../services/LocalStorage';


class Card extends Component{

    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            favorite: false
        };

        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.handleClickToCart = this.handleClickToCart.bind(this);
        this.handleClickToFavorite = this.handleClickToFavorite.bind(this);
    }

    handleClickToCart() {
        const {data} = this.props;

        this.handleToggleModal();
        LocalStorage.postCardToLocalStorage('cart', data);
    }

    handleClickToFavorite() {
        const {data} = this.props;

        if(!this.state.favorite) {
            LocalStorage.postCardToLocalStorage('favorite', data);
        } else {
            LocalStorage.deleteCardFromLocalStorage('favorite', data.article);
        }

        this.handleToggleFavorite();
    }

    handleToggleFavorite() {
        this.setState({favorite: !this.state.favorite});
    }

    handleToggleModal() {
        this.setState({showModal: !this.state.showModal});
    }

    componentDidMount() {
        let arrayData = LocalStorage.getCardsFromLocalStorage('favorite');

        if (arrayData) {
            const elem = arrayData.find(element => {return element.article === this.props.data.article});
            if (elem) {
                this.handleToggleFavorite();
            }
        }
    }

    render() {
        const {data, classes} = this.props;
        const iconFavorite = <i className="fas fa-star"/>;
        const {showModal} = this.state;
        const buttonsModal = [
            <Button key="1" additionClass="btnOk" text="Ok" onClick={this.handleClickToCart}/>,
            <Button key="2" additionClass="btnCancel" text="Cancel" onClick={this.handleToggleModal}/>
        ];


        return(

            <div className={classes.card}>
                <img className={classes.cardImage} src={data.url} alt={data.name} width="100"/>
                <h3 className={classes.cardTitle}>
                    {data.name}
                </h3>
                <p className={classes.cardArticle}>
                    Article: {data.article}
                </p>
                <div className={classes.cardPrice}>
                    {data.price} $
                </div>
                <div className={classes.cardButtonsContainer}>
                    <Button additionClass={this.state.favorite ? "btnAddToFavoriteTrue" : "btnAddToFavoriteFalse"}
                            text={iconFavorite}
                            onClick={this.handleClickToFavorite}/>
                    <Button additionClass="btnAddToCard" text="Add to Card" onClick={this.handleToggleModal}/>
                    {showModal && <Modal
                        header="Are yor sure?"
                        closeButton="true"
                        text="Do you want to add this product to the cart?"
                        actions={buttonsModal}
                        onClose={this.handleToggleModal}/>}
                </div>
            </div>
        );
    }
}

Card.propTypes = {
    data: PropTypes.object,
    classes: PropTypes.object
};

export default injectSheet(styles)(Card);