export default {

    btn: {
        fontSize: '14px',
        border: 'none',
        borderRadius: '5px',
        cursor: 'pointer',
    },

    btnAddToCard: {
        padding: '8px',
        backgroundColor: 'rgb(0, 0, 0)',
        color: 'white',

        '&:hover': {
            backgroundColor: 'rgba(48, 164, 91)',
            color: 'rgb(0, 0, 0)',
        }
    },

    btnAddToFavoriteFalse: {
        padding: 0,
        color: 'rgb(255, 255, 255)',
        backgroundColor: 'rgba(48, 66, 75, 0.55)',

        '&:hover': {
            color: 'rgb(0, 0, 0)',
        }
    },

    btnAddToFavoriteTrue: {
        padding: 0,
        color: 'rgb(0, 0, 0)',
        backgroundColor: 'rgba(48, 66, 75, 0.55)',

        '&:hover': {
            color: 'rgb(255, 255, 255)',
        }
    },

    btnOk: {
        padding: '8px',
    },

    btnCancel: {
        padding: '8px',
    }
}