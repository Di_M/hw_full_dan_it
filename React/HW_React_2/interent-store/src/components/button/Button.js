import React, {Component} from 'react';
import injectSheet from "react-jss";
import styles from "./ButtonStyles";
import PropTypes from "prop-types";

class Button extends Component{

    findAdditionalClass() {
        const {classes, additionClass} = this.props;
        for (let prop in classes) {
            if(prop === additionClass) {
                return classes[prop];
            }
        }
        return false;
    }

    render() {
        const {backgroundColor, text, onClick, classes} = this.props;
        const addClass = this.findAdditionalClass();

        return (
            <button
                className={`${classes.btn} ${addClass}`}
                style={{backgroundColor: backgroundColor}}
                type="button"
                onClick={onClick}>
                {text}
            </button>
        );
    }
}

Button.propTypes = {
    additionClass: PropTypes.string,
    classes: PropTypes.object,
    backgroundColor: PropTypes.string,
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    onClick: PropTypes.func
};

export default injectSheet(styles)(Button);