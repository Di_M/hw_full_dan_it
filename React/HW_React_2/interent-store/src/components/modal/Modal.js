import React, {Component} from 'react';
import Button from "../button/Button";
import injectSheet from "react-jss";
import styles from "./ModalStyles";
import PropTypes from 'prop-types';
import isNil from "lodash/fp/isNil";

class Modal extends Component{
    constructor() {
        super();

        this.handlerCloseModal = this.handlerCloseModal.bind(this);
    }

    componentDidMount() {
        document.addEventListener("click", this.handlerCloseModal, false);
    }

    componentWillUnmount() {
        document.removeEventListener("click", this.handlerCloseModal, false);
    }



    handlerCloseModal(e) {
        const {onClose} = this.props;

        if (!isNil(this.modal)) {
            if (!this.modal.contains(e.target)) {
                onClose();
            }
        }
    }

    render() {
        const {header, closeButton, text, actions, classes, onClose} = this.props;
        return (
            <div className={classes.modal}>
                <div className={classes.modalContainer} ref={node => (this.modal = node)}>
                    <div className={classes.modalHeader}>
                        <h2 className={classes.modalHeaderTitle}>
                            {header}
                        </h2>
                        {closeButton === "true" &&
                        <div className={classes.modalBtnClose}>
                            <Button text="X" onClick={onClose}/>
                        </div>
                        }
                    </div>
                    <div className={classes.modalBody}>
                        <p className={classes.modalBodyText}>
                            {text}
                        </p>
                    </div>
                    {actions.length !== 0 &&
                    <div className={classes.modalFooter}>
                        {actions}
                    </div>
                    }
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    classes: PropTypes.object,
    header: PropTypes.string,
    closeButton: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.array,
    onClose: PropTypes.func,
};

export default injectSheet(styles)(Modal);