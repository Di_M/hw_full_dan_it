import React, {Component} from 'react';
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./AppStyles";
import ListCards from '../../components/list-cards/ListCards';


class App extends  Component{

    render() {
        const {classes} = this.props;

        return (
            <div className={classes.app}>
                <ListCards />
            </div>
        )
    }
}

App.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(App);
