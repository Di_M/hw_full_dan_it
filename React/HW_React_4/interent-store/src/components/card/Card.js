import React, {useEffect, useState} from 'react';
import { useDispatch, useSelector } from "react-redux";
import injectSheet from "react-jss";
import styles from "./CardStyle";
import PropTypes from 'prop-types';
import Button from '../button/Button';
import Modal from "../modal/Modal";
import LocalStorage from '../../services/LocalStorage';
import * as favoriteListActions from "../../actions/favoriteListActions";
import * as cartListActions from "../../actions/cartListActions";


function Card(props) {

    let {data, classes, addedToCart, toggleModal} = props;
    let [favorite, setFavorite] = useState(false);
    let [itemInCart, setItemInCart] = useState(addedToCart);
    let [isModalOpen, setIsModalOpen] = useState(false);

    const favoriteList = useSelector(state => state.favoriteListReducer.favoriteList);
    const cartList = useSelector(state => state.cartListReducer.cartList);
    const dispatch = useDispatch();

    useEffect( () => {
        if (favoriteList) {
            const elem = favoriteList.find(element => {return element.article === data.article});
            if (elem) {
                setFavorite(true);
            }
        }
    }, [data.article, favoriteList]);

    useEffect( () => {
        if (cartList) {
            const elem = cartList.find(element => {return element.article === data.article});
            if (elem) {
                setItemInCart(true);
            }
        }
    }, [data.article, cartList]);

    const handleClickToCart = () => {
        LocalStorage.postCardToLocalStorage('cart', data);
        dispatch(cartListActions.addItem(data));
        setItemInCart(!itemInCart);
        handleToggleModal();
    };

    const handleDeleteFromCart = () => {
        LocalStorage.deleteCardFromLocalStorage('cart', data.article);
        setItemInCart(!itemInCart);

        let newList = cartList.filter(elem => elem.article !== data.article);
        dispatch(cartListActions.updateList([...newList]));

        handleToggleModal();
    };

    const handleClickToFavorite = () => {
        if(!favorite) {
            LocalStorage.postCardToLocalStorage('favorite', data);
            dispatch(favoriteListActions.addItem(data));
        } else {
            LocalStorage.deleteCardFromLocalStorage('favorite', data.article);

            let newList = favoriteList.filter(elem => elem.article !== data.article);
            dispatch(favoriteListActions.updateList([...newList]));
        }

        handleToggleFavorite();
    };

    const handleToggleFavorite = () => {
        setFavorite(!favorite);
    };

    const handleToggleModal = () => {
        setIsModalOpen(!toggleModal());
    };

    const iconFavorite = <i className="fas fa-star"/>;
    const buttonsDefaultModal = [
        <Button key="1" additionClass="btnOk" text="Ok" onClick={handleClickToCart}/>,
        <Button key="2" additionClass="btnCancel" text="Cancel" onClick={handleToggleModal}/>
    ];
    const buttonsDeleteModal = [
        <Button key="1" additionClass="btnOk" text="Ok" onClick={handleDeleteFromCart}/>,
        <Button key="2" additionClass="btnCancel" text="Cancel" onClick={handleToggleModal}/>
    ];

    return(

        <div className={classes.card}>
            <img className={classes.cardImage} src={data.url} alt={data.name} width="100"/>
            <h3 className={classes.cardTitle}>
                {data.name}
            </h3>
            <p className={classes.cardArticle}>
                Article: {data.article}
            </p>
            <div className={classes.cardPrice}>
                {data.price} $
            </div>
            <div className={classes.cardButtonsContainer}>
                <Button additionClass={favorite ? "btnAddToFavoriteTrue" : "btnAddToFavoriteFalse"}
                        text={iconFavorite}
                        onClick={handleClickToFavorite}/>
                {!itemInCart && <Button additionClass="btnAddToCard" text="Add to Card" onClick={handleToggleModal}/>}
                {isModalOpen && !itemInCart && <Modal
                    header="Are yor sure?"
                    closeButton={true}
                    text="Do you want to add this product to the cart?"
                    actions={buttonsDefaultModal}
                    onClose={handleToggleModal}/>}
                {itemInCart && <Button additionClass="btnRemove" text="Remove" onClick={handleToggleModal}/>}
                {isModalOpen && itemInCart && <Modal
                    header="Are yor sure?"
                    closeButton={true}
                    text="Do you want to remove this product from the cart?"
                    actions={buttonsDeleteModal}
                    onClose={handleToggleModal}/>}
            </div>
        </div>
    );
}

Card.propTypes = {
    data: PropTypes.object,
    classes: PropTypes.object,
    toggleModal: PropTypes.func,
    addedToCart: PropTypes.bool
};

export default injectSheet(styles)(Card);