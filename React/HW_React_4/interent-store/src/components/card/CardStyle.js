export default {
    card: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: '200px',
        margin: '0 10px 20px 10px',
        padding: '5px',
        backgroundColor: 'rgba(48, 66, 75)',
        border: '1px solid',
        borderColor: 'rgb(38, 27, 35)',
        borderRadius: '4px',
    },

    cardImage: {
        width: '100%',
        height: '200px',
        marginBottom: '10px'
    },

    cardTitle: {
        height: '50px',
        margin: 0,
        marginBottom: '10px',
        fontSize: '16px',
        color: 'rgb(0, 0, 0)'
    },

    cardArticle: {
        margin: 0,
        marginBottom: '10px',
    },

    cardPrice: {
        marginBottom: '10px',
    },

    cardButtonsContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
}