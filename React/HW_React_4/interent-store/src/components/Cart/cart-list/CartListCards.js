import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./CartListCardsStyles";
import Card from "../../card/Card";
import * as modalActions from "../../../actions/modalActions";

function CartListCards(props) {

    const {classes} = props;
    const cartList = useSelector(state => state.cartListReducer.cartList);
    const isOpenModal = useSelector(state => state.modalReducer);
    const dispatch = useDispatch();

    const handleToggleModal = () => {
        dispatch(modalActions.showModal());
        return isOpenModal;
    };

    const newCartItem = cartList.map((item, i) => (<Card key={item+i} data={item} addedToCart={true} toggleModal={handleToggleModal}/>));

    return (
        <div className={classes.listCards}>
            {newCartItem}
        </div>
    );
}

CartListCards.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(CartListCards);