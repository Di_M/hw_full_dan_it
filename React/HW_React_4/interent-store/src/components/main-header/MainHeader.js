import React from 'react';
import {Link} from "react-router-dom";
import injectSheet from "react-jss";
import PropTypes from 'prop-types';
import styles from "./MainHeaderStyles";
import {routes} from '../../constants/constRouters';

function MainHeader({classes}) {

    return (
        <header className={classes.header}>
            <ul className={classes.headerList}>
                <li className={classes.headerListItem}><Link to={routes.home.href}>{routes.home.name}</Link></li>
                <li className={classes.headerListItem}><Link to={routes.cart.href}>{routes.cart.name}</Link></li>
                <li className={classes.headerListItem}><Link to={routes.favorites.href}>{routes.favorites.name}</Link></li>
            </ul>
        </header>
    )
}

MainHeader.propTypes = {
    classes: PropTypes.object
};

export default injectSheet(styles)(MainHeader);