import {Component} from './components.js';
import {FormCeateTasksList} from './form_create_list.js';
import {TasksList} from './task_list.js';

class Trello extends Component{
    _todoListContainer;
    _todoList;
    _createFormList;

    constructor() {
        super();
        this._createFormList = new FormCeateTasksList();
        this._createLayout();
        this.appendTo(document.body);
        this.addNewTasksListListener();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('trello_container');
        this._createFormList.appendTo(this._element);

        this._todoListContainer = document.createElement('div');
        this._todoListContainer.classList.add('trello_container-task-list-container');
        this._element.append(this._todoListContainer);
    }

    addNewTasksListListener() {
        this._createFormList._btnAddNewList.addEventListener('click', () => {
            if (this._createFormList._nameList.value !== '') {
                this._todoList = new TasksList(this._createFormList._nameList.value);
                this._todoList.appendTo(this._todoListContainer);
                this._createFormList._nameList.value = '';
            } else {
                alert('Type something');
            }
        });
    }
}

const taskField = new Trello();
taskField._createFormList._todoList = new TasksList("TO DO");
taskField._createFormList._todoList.appendTo(taskField._todoListContainer);