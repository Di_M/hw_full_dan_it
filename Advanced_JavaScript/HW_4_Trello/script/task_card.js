import {Component} from "./components.js";

export class TaskCardContainer extends Component {
    _taskCard;
    _taskText;
    _buttonDestroyTaskCard;
    _dragEl = null;

    constructor() {
        super();
        this._createLayout();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('trello_tasks-card-container');
    }

    _createTaskCard(val) {
        this._taskCard = document.createElement('div');
        this._taskCard.classList.add('trello_tasks-card');
        this._taskCard.setAttribute('draggable', 'true');

        this._taskText = document.createElement('p');
        this._taskText.innerText = `${val}`;
        this._taskCard.append(this._taskText);

        this._element.append(this._taskCard);
    }

    _createTaskCardDestroyerButton() {
        this._buttonDestroyTaskCard = document.createElement('button');
        this._buttonDestroyTaskCard.classList.add('trello_tasks-card-destroyer-btn');
        this._buttonDestroyTaskCard.innerText = 'X';
        this._taskCard.append(this._buttonDestroyTaskCard);
    }

    _destroyTaskCardListener() {
        this._element.addEventListener('click', (e) => {
            e.preventDefault();
            if (e.target.classList.contains('trello_tasks-card-destroyer-btn')) {
                e.target.parentNode.remove();
            }
        });
    }

    sortTasksCard() {
        let listArray = [];
        this._element.childNodes.forEach(item => {listArray.push(item)});
        listArray.sort((a, b) => (a.innerText.toLowerCase() > b.innerText.toLowerCase()) ? 1 : -1 );
        this._element.innerHTML = '';
        listArray.forEach(item => this._element.append(item));
    }


    //Drag and drop events
    _dragStart(e) {
        this._dragEl = e.target;
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', e.target.outerHTML);
    };

    _dragEnter(e) {
        if(e.target.parentNode.classList.contains('trello_tasks-card')) {
            e.target.parentNode.classList.add('over');
        }
    }

    _dragOver(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        e.dataTransfer.dropEffect = 'move';
        if (this._dragEl !== e.target &&
            this._element.contains(this._dragEl) &&
            e.target.parentNode.classList.contains('trello_tasks-card')) {

            e.target.parentNode.classList.add('over');

            let checkSibling = this._checkSibling(this._dragEl, e.target.parentNode);
            if(checkSibling) {
                this._element.insertBefore(this._dragEl, e.target.parentNode);
            } else {
                this._element.insertBefore(this._dragEl, e.target.parentNode.nextSibling);
            }
        }
        return false;
    }

    _dragLeave(e) {
        e.stopPropagation();
        if(e.target.parentNode.classList.contains('trello_tasks-card')) {
            e.target.parentNode.classList.remove('over');
        }
    }

    _dragDrop(e) {
        e.stopPropagation();
        if(e.target.parentNode.classList.contains('trello_tasks-card')) {
            e.target.parentNode.classList.remove('over');
        }
        return false;
    }

    _dragEnd() {
        [].forEach.call(this._element.childNodes, (item) => {
            item.classList.remove('over');
        });
        this._dragEl = null;
    }

    _checkSibling(el1, el2) {
        let current;
        if (el2.parentNode === el1.parentNode) {
            for(current = el1.previousSibling; current; current = current.previousSibling ) {
                if (current === el2) return true
            }
        } else return false;
    }

    _EventsDragAndDrop(el) {
        el.addEventListener('dragstart', this._dragStart.bind(this), false);
        el.addEventListener('dragenter', this._dragEnter, false);
        el.addEventListener('dragover', this._dragOver.bind(this), false);
        el.addEventListener('dragleave', this._dragLeave, false);
        el.addEventListener('drop', this._dragDrop, false);
        el.addEventListener('dragend', this._dragEnd.bind(this), false);
    }

    addDragAndDropToTasksCard() {
        [].forEach.call(this._element.childNodes, (item) => {
            this._EventsDragAndDrop(item);
        });
    }

}