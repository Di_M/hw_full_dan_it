//
const urlUsers = 'https://jsonplaceholder.typicode.com/users';
const urlPosts = 'https://jsonplaceholder.typicode.com/posts';
let dataUsers = null;
let dataPosts = null;



//Supporting functions
const findEmail = (email) => {
    let idUser;
    dataUsers.forEach((elem) => {
        if(elem.email === email) {
            idUser = elem.id;
        }
    });
    return idUser;
};


const findPostId = (idUser, title) => {
    let postId;
    dataPosts.forEach((elem) => {
        if(elem.userId === idUser && elem.title === title) {
            postId = elem.id;
        }
    });
    return postId;
};


const createPost = (arrayPosts, arrayUsers) => {
    arrayPosts.forEach((el, i) => {
        const cardTitle = $('<h3></h3>').addClass('card-title').text(`${el.title}`);
        const cardPost = $('<p></p>').addClass('card-post').text(`${el.body}`);

        arrayUsers.forEach((element) => {
            if (el.userId === element.id) {
                const cardContainer = $('<div></div>').addClass('card-container');
                const cardUserNameAndSurname = $('<p></p>').addClass('card-user-name').text(`${element.name}`);
                const cardUserEmail = $('<a></a>')
                    .attr('href', `mailto:${element.email}`)
                    .addClass('card-user-email')
                    .text(`${element.email}`);
                const cardButtonContainer = $('<div></div>').addClass('card-btn-container');
                const buttonEdit = $('<button></button>').addClass('btn card-btn-edit').text('Edit Post');
                const buttonDelete = $('<button></button>').addClass('btn card-btn-delete').text('Delete Post');

                cardButtonContainer.append(buttonEdit).append(buttonDelete);

                cardContainer.append(cardUserNameAndSurname);
                cardContainer.append(cardUserEmail);
                cardContainer.append(cardButtonContainer);

                cardContainer.append(cardTitle);
                cardContainer.append(cardPost);

                $('.container').prepend(cardContainer);
            }
        });

        if (i === arrayPosts.length - 1) {
            setTimeout(function () {
                $('.animation-container').removeClass('show').addClass('hide');
                $("html,body").css("overflow", "auto");
            }, 3000);
        }
    });
};


const getRequestFromURLUsers = (url) => {
    $.ajax({
        url: url,
        async: false,
        success: (data) => {
            console.log(data);
            dataUsers = data;
        }
    });
};

const getRequestFromURLPosts = (url) => {
    $.ajax({
        url: url,
        async: false,
        success: (posts) => {
            console.log(posts);
            dataPosts = posts;
        }
    });
};


const newPostListener = () => {
    $('.btn-add-new-post').on('click', function (e) {
        e.preventDefault();

        const userNameText = dataUsers[0].name;
        const userEmailText = dataUsers[0].email;

        $('.modal_name-input').val(userNameText);
        $('.modal_email-input').val(userEmailText);
        $('.modal_title-input').val('');
        $('.modal_description-area').val('');

        $('.modal').css('display', 'flex');

        $('.modal_form-btn-accept').on('click', function (event) {
            event.preventDefault();

            if ($('.modal_name-input').val() !== '' && $('.modal_email-input').val() !== ''
                && $('.modal_title-input').val() !== '' && $('.modal_description-area').val() !== '') {
                const userName = $('.modal_name-input').val();
                const userEmail = $('.modal_email-input').val();
                const postTitle = $('.modal_title-input').val();
                const postDescription = $('.modal_description-area').val();

                const postObj = {};
                postObj.userId = dataUsers[0].id;
                postObj.id = dataPosts.length + 1;
                postObj.title = postTitle;
                postObj.body = postDescription;

                dataPosts.push(postObj);
                console.log(dataPosts);

                const cardContainer = $('<div></div>').addClass('card-container');
                const cardUserNameAndSurname = $('<p></p>').addClass('card-user-name').text(userName);
                const cardUserEmail = $('<a></a>')
                    .attr('href', `mailto:${userEmail}`)
                    .addClass('card-user-email')
                    .text(userEmail);
                const cardButtonContainer = $('<div></div>').addClass('card-btn-container');
                const buttonEdit = $('<button></button>').addClass('btn card-btn-edit').text('Edit Post');
                const buttonDelete = $('<button></button>').addClass('btn card-btn-delete').text('Delete Post');
                const cardTitle = $('<h3></h3>').addClass('card-title').text(postTitle);
                const cardPost = $('<p></p>').addClass('card-post').text(postDescription);

                cardButtonContainer.append(buttonEdit).append(buttonDelete);

                cardContainer.append(cardUserNameAndSurname);
                cardContainer.append(cardUserEmail);
                cardContainer.append(cardButtonContainer);

                cardContainer.append(cardTitle);
                cardContainer.append(cardPost);

                $('.container').prepend(cardContainer);
                $.ajax({
                    url: urlPosts,
                    method: 'POST',
                    data: JSON.stringify(postObj),
                    dataType: 'json',
                }).done(response => console.log("Post response", response));

                $('.modal').css('display', 'none');
                $(this).off();
            } else {
                alert('Please fill all fields');
            }
        })
    });
};


const editPostListener = () => {
    $('.container').on('click', function (e) {
        e.preventDefault();
        if (e.target && $(e.target).hasClass('card-btn-edit')) {
            const nameText = $(e.target).parent().siblings('.card-user-name').text();
            const emailText = $(e.target).parent().siblings('.card-user-email').text();
            const title = $(e.target).parent().siblings('.card-title');
            const description = $(e.target).parent().siblings('.card-post');

            $('.modal').css('display', 'flex');
            $('.modal_name-input').val(nameText);
            $('.modal_email-input').val(emailText);
            $('.modal_title-input').val(title.text());
            $('.modal_description-area').val(description.text());


            $('.modal_form-btn-accept').on('click', function (event) {
                event.preventDefault();
                if ($('.modal_title-input').val() !== '' && $('.modal_description-area').val() !== '') {
                    const PostId = findPostId(findEmail(emailText), title.text());
                    const currentPost = dataPosts.find(post => post.id === PostId);
                    currentPost.title = $('.modal_title-input').val();
                    currentPost.body = $('.modal_description-area').val();

                    $.ajax({
                        url: `${urlPosts}/${PostId}`,
                        method: 'PUT',
                        data: JSON.stringify(currentPost),
                        dataType: 'json',
                    }).done(response => console.log("Put response", response));

                    title.text($('.modal_title-input').val());
                    description.text($('.modal_description-area').val());

                    $('.modal').css('display', 'none');
                    $(this).off();
                } else {
                    alert('Please fill all fields');
                }
            })
        }
    });
};


const deletePostListener = () => {
    $('.container').on('click', function (e) {
        e.preventDefault();
        if (e.target && $(e.target).hasClass('card-btn-delete')) {
            const email = $(e.target).parent().siblings('.card-user-email').text();
            const title = $(e.target).parent().siblings('.card-title').text();

            const PostId = findPostId(findEmail(email), title);
            const currentPost = dataPosts.find(post => post.id === PostId);


            $.ajax({
                url: `${urlPosts}/${PostId}`,
                method: 'DELETE',
            }).done(response => console.log("Delete response", response));

            dataPosts.splice(currentPost.id - 1, 1);
            console.log(dataPosts);

            $(e.target).parent().parent().remove();
        }
    });
};

//Show Animation
$('.animation-container').removeClass('hide').addClass('show');
$("html,body").css("overflow", "hidden");


// GET requests from "urlUsers" and "urlPosts"
getRequestFromURLUsers(urlUsers);
getRequestFromURLPosts(urlPosts);


// Create Users Posts
createPost(dataPosts, dataUsers);


// Create new Post events
newPostListener();


// Edit Post events
editPostListener();


// Delete Post events
deletePostListener();


//Modal close

$('.modal_form-btn-close').on('click', function (e) {
    e.preventDefault();
    $('.modal').css('display', 'none');
    $('.modal_form-btn-accept').off();
});