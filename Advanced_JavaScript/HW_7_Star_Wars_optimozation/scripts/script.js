const btnShowContent = document.querySelector('.btn-show-films');
const containerForStarWarsInfo = document.querySelector('.content-container');
let clickedBtn = false;

//Create Episodes Star Wars
const createEpisode = (episode) => {

    const divEpisode = document.createElement('div');
    divEpisode.classList.add('list');

    const episodeNumber = document.createElement('h2');
    episodeNumber.classList.add('list-number');
    episodeNumber.innerText = `${episode.episode_id}`;
    divEpisode.append(episodeNumber);

    const titleEpisode = document.createElement('h2');
    titleEpisode.classList.add('list-title');
    titleEpisode.innerText = `${episode.title}`;
    divEpisode.append(titleEpisode);

    const openingCrawlEpisode = document.createElement('p');
    openingCrawlEpisode.classList.add('list-descr');
    openingCrawlEpisode.innerText = `${episode.opening_crawl}`;
    divEpisode.append(openingCrawlEpisode);

    containerForStarWarsInfo.append(divEpisode);

    return [titleEpisode, divEpisode];
};


// Fetch requests to swapi.co
const showStarWarsInfo = () => {

    fetch('https://swapi.co/api/films/')
        .then((response) => response.json())
        .then((result) => {
            let resultsArray = result.results;
            console.log(resultsArray);
            resultsArray.sort((a, b) => {return a.episode_id - b.episode_id});
            resultsArray.forEach((item) => {

                let [titleEpisode, divEpisode] = createEpisode(item);

                titleEpisode.classList.remove('list-title');
                titleEpisode.classList.add('animate-title-film');

                const listCharacters = document.createElement('ul');
                listCharacters.classList.add('list-characters');

                let resultArray = [];

                item.characters.forEach((element) => {
                    let promise = fetch(`${element}`)
                        .then((response) => response.json());
                    resultArray.push(promise);
                });

                Promise.all(resultArray).then(values => {
                    values.forEach( element => {
                        let characterFromRequest = element.name;
                        const characterToShow = document.createElement('li');
                        characterToShow.classList.add('list-item-characters');
                        characterToShow.innerText = `${characterFromRequest}`;
                        listCharacters.append(characterToShow);
                    });
                    titleEpisode.classList.remove('animate-title-film');
                    titleEpisode.classList.add('list-title');
                });

                divEpisode.append(listCharacters);
            });
        });

};


//Show Content Event
btnShowContent.addEventListener('click', () => {
    if (clickedBtn === false) {
        clickedBtn = true;
        showStarWarsInfo();
    }
});
