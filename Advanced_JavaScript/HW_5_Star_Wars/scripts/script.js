const btnShowContent = document.querySelector('.btn-show-films');
const containerForStarWarsInfo = document.querySelector('.content-container');
let clickedBtn = false;

//Show Content Event
btnShowContent.addEventListener('click', () => {
    if (clickedBtn === false) {
        clickedBtn = true;
        showStarWarsInfo();
    }
});

//XMLHttpRequest for StarWars episode_id, title, opening_crawl, characters
const showStarWarsInfo = () => {
    let requestStarWarsInfo = new XMLHttpRequest();

    requestStarWarsInfo.open('GET', 'https://swapi.co/api/films/', true);
    requestStarWarsInfo.setRequestHeader('Content-type', 'application/json; charset=utf8');
    requestStarWarsInfo.responseType = 'json';

    requestStarWarsInfo.addEventListener('readystatechange', () => {

        if(requestStarWarsInfo.readyState === 4 && requestStarWarsInfo.status === 200) {
            let results = requestStarWarsInfo.response.results;
            // Sort Episodes from 1 to last for correct display
            results.sort((a, b) => {return a.episode_id - b.episode_id});
            results.forEach((item) => {

                const divEpisode = document.createElement('div');
                divEpisode.classList.add('list');

                const episodeNumber = document.createElement('h2');
                episodeNumber.classList.add('list-number');
                episodeNumber.innerText = `${item.episode_id}`;
                divEpisode.append(episodeNumber);

                const titleEpisode = document.createElement('h2');
                titleEpisode.classList.add('list-title');
                titleEpisode.innerText = `${item.title}`;
                divEpisode.append(titleEpisode);

                const openingCrawlEpisode = document.createElement('p');
                openingCrawlEpisode.classList.add('list-descr');
                openingCrawlEpisode.innerText = `${item.opening_crawl}`;
                divEpisode.append(openingCrawlEpisode);

                containerForStarWarsInfo.append(divEpisode);

                const listCharacters = document.createElement('ul');
                listCharacters.classList.add('list-characters');

                let charactersRequestsAmount = item.characters.length; // Number of requests which we have to send
                let counterReceivedCharactersRequests = 0;

                item.characters.forEach((element) => {
5
                    let requestCharacters = new XMLHttpRequest();

                    requestCharacters.open('GET', `${element}`, true);
                    requestCharacters.setRequestHeader('Content-type', 'application/json; charset=utf8');
                    requestCharacters.responseType = 'json';

                    titleEpisode.classList.remove('list-title');
                    titleEpisode.classList.add('animate-title-film');

                    requestCharacters.addEventListener('readystatechange', () => {

                        if(requestCharacters.readyState === 4 && requestCharacters.status === 200) {
                            counterReceivedCharactersRequests++;

                            let characterFromRequest = requestCharacters.response.name;
                            const characterToShow = document.createElement('li');
                            characterToShow.classList.add('list-item-characters');
                            characterToShow.innerText = `${characterFromRequest}`;
                            listCharacters.append(characterToShow);

                            if(counterReceivedCharactersRequests === charactersRequestsAmount) {
                                titleEpisode.classList.remove('animate-title-film');
                                titleEpisode.classList.add('list-title');
                            }
                        }
                    });

                    requestCharacters.send();
                });

                divEpisode.append(listCharacters);
            });
        }
    });




    requestStarWarsInfo.send();
};
