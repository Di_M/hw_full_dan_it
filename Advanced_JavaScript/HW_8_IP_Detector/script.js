const btnIPDetector = document.querySelector('.btn-ip-detector');
const container = document.querySelector('.container');
let clickedBtn = false;


// Helper function
const getInfo = (url) => {
   return fetch(url).then((response) => response.json());
};

const createLayout = (values) => {
    for (let key in values) {
        if(values[key] !== '') {
            let elem = document.createElement('p');
            elem.innerText = `${values[key]}`;

            container.append(elem);
        }
    }
};

const showInfoFromIP = async () => {
    let ip = await getInfo('https://api.ipify.org/?format=json');

    console.log('IP', ip);

    let result = await getInfo(`http://ip-api.com/json/${ip.ip}?lang=ru&fields=continent,country,regionName,city,district`);

    console.log('Result', result);
    createLayout(result);
};



// Events
btnIPDetector.addEventListener('click', ()=> {
    if (clickedBtn === false) {
        clickedBtn = true;
        showInfoFromIP();
    }
});
