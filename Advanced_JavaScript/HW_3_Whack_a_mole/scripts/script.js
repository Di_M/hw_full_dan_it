class Component {
    _element = null;

    constructor() {
    }
    appendTo(container) {
        container.append(this._element);
    }
}

class Game extends Component{
    _gameOptions = null;
    _gameTable = null;

    constructor() {
        super();
        this._gameOptions = new GameOptions();
        this._gameTable = new GameTable();
        this._createLayout();
        this.appendTo(document.body);
        this.startGame();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._gameOptions.appendTo(this._element);
        this._gameTable.appendTo(this._element);
    }

    startGame() {
        this._gameOptions._startBtn._element.addEventListener('click', (event) => {
            event.target.classList.add('start-btn--invisible');
            this._gameOptions._levels._inputs.forEach(item => item.disabled = true);
            event.preventDefault();
            this._gameOptions._levels.chooseLevel();
            this._gameOptions._score.reset();
            this._setCorrectCellClickListener();
            this.intervalFunctionForCells(this._gameOptions._levels._chooseLevel);
        });
    }

    _setCorrectCellClickListener() {
        this._gameTable._element.addEventListener('click', (event) => {
            if(event.target.classList.contains('active-cell')) {
                event.target.classList.remove('active-cell');
                event.target.classList.add('correct-cell');
                this._gameOptions._score.incrementPlayer();
            }
        });
    }

    intervalFunctionForCells(val) {
        this._setIntervalToColorCells = setInterval(this._randomizeCellsToColor.bind(this), val);
    }

    _randomizeCellsToColor() {
        this._setFailCell();
        this._gameTable._setRandomCellActive();
        this._checkWinnerInGame();
    };

    _setFailCell() {
        this._gameTable.cells.forEach(item => {
            if(item.classList.contains('active-cell')) {
                item.classList.remove('active-cell');
                item.classList.add('fail-cell');
                this._gameOptions._score.incrementComputer();
            }
        });
    }

    _checkWinnerInGame() {
        if(this._gameTable.cells.filter(item => item.classList.contains('fail-cell')).length === this._gameTable.cells.length / 2) {
            clearInterval(this._setIntervalToColorCells);
            alert(`You loose! Computer win!!!)))\nIt\`s score: ${this._gameOptions._score.scoreComputer}`);
            this._gameTable.clearTable();
            this._gameOptions._startBtn._element.classList.remove('start-btn--invisible');
            this._gameOptions._levels._inputs.forEach(item => item.disabled = false);
        } else if(this._gameTable.cells.filter(item => item.classList.contains('correct-cell')).length === this._gameTable.cells.length  / 2) {
            clearInterval(this._setIntervalToColorCells);
            alert(`Congratulation. You Win!\nYour score: ${this._gameOptions._score.scorePlayer}`);
            this._gameTable.clearTable();
            this._gameOptions._startBtn._element.classList.remove('start-btn--invisible');
            this._gameOptions._levels._inputs.forEach(item => item.disabled = false);
        }
    }
}

class GameTable extends Component{
    cells = [];
    numRows = 10;
    numCellsInRows = 10;

    constructor() {
        super();
        this._createLayout();
    }

    clearTable() {
        this.cells.forEach(item => {
            item.className = 'table-game-cells';
        });
    }

    _createLayout() {
        const tableGame = document.createElement('table');
        tableGame.classList.add('table-game');

        for (let i = 0; i < this.numRows; i++) {
            const tableGameRow = document.createElement('tr');
            for(let j = 0; j < this.numCellsInRows; j++) {
                const tableGameCell = document.createElement('td');
                tableGameCell.classList.add('table-game-cells');
                this.cells.push(tableGameCell);
                tableGameRow.append(tableGameCell);
            }
            tableGame.append(tableGameRow);
        }

        this._element = tableGame;
    }

    _setRandomCellActive() {
        let num;
        do {
            num = Math.floor(Math.random() * (this.numRows * this.numCellsInRows));
        } while(this.cells[num].classList.contains('active-cell') ||
        this.cells[num].classList.contains('correct-cell') ||
        this.cells[num].classList.contains('fail-cell'));

        this.cells[num].classList.add('active-cell');
    }
}

class GameOptions extends Component{
    _startBtn = null;
    _levels = null;
    _score = null;

    constructor() {
        super();
        this._startBtn = new StartBtn();
        this._levels = new Levels();
        this._score = new Score();
        this._createLayout();

    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('game-options');
        this._startBtn.appendTo(this._element);
        this._levels.appendTo(this._element);
        this._score.appendTo(this._element);
    }
}

class StartBtn extends Component{
    constructor() {
        super();
        this._createLayout();
    }

    _createLayout() {
        const startBtn = document.createElement('button');
        startBtn.innerText = 'Start';
        startBtn.classList.add('start-btn');

        this._element = startBtn;
    }
}

class Levels extends Component{
    _inputs = [];
    _chooseLevel = null;

    constructor() {
        super();
        this._createLayout();
    }
    _createLayout() {
        const levelsContainer = document.createElement('div');
        levelsContainer.classList.add('levels-container');

        const labelBeginer = document.createElement('label');
        const levelBeginer = document.createElement('input');
        levelBeginer.setAttribute('type', 'radio');
        levelBeginer.setAttribute('name', 'level');
        levelBeginer.setAttribute('value', '1500');
        levelBeginer.setAttribute('checked', 'checked');
        this._inputs.push(levelBeginer);
        labelBeginer.innerText = 'Любитель';
        labelBeginer.append(levelBeginer);
        levelsContainer.append(labelBeginer);

        const labelProfi = document.createElement('label');
        const levelProfi = document.createElement('input');
        levelProfi.setAttribute('type', 'radio');
        levelProfi.setAttribute('name', 'level');
        levelProfi.setAttribute('value', '1000');
        this._inputs.push(levelProfi);
        labelProfi.innerText = 'Профи';
        labelProfi.append(levelProfi);
        levelsContainer.append(labelProfi);

        const labelMaster = document.createElement('label');
        const levelMaster = document.createElement('input');
        levelMaster.setAttribute('type', 'radio');
        levelMaster.setAttribute('name', 'level');
        levelMaster.setAttribute('value', '500');
        this._inputs.push(levelMaster);
        labelMaster.innerText = 'Мастер';
        labelMaster.append(levelMaster);
        levelsContainer.append(labelMaster);


        this._element = levelsContainer;
    }

    chooseLevel() {
        this._inputs.forEach(item => {
            if(item.checked) {
                this._chooseLevel = +item.value;
            }
        });
    }
}

class Score extends  Component{
    scorePlayer = 0;
    scoreComputer = 0;

    constructor() {
        super();
        this._createLayout();
    }

    incrementPlayer() {
        this.scorePlayer++;
        this._updateLayout();
    }

    incrementComputer() {
        this.scoreComputer++;
        this._updateLayout();
    }

    reset() {
        this.scorePlayer = 0;
        this.scoreComputer = 0;
        this._updateLayout();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._updateLayout();
    }
    _updateLayout() {
        this._element.innerHTML = `Score player: ${this.scorePlayer} <br> Score computer: ${this.scoreComputer}`;
    }
}

const game = new Game();