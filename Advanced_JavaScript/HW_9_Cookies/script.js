const btnContactUs = document.querySelector('.btn-black');


const getCookie = (cookieName) => {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + cookieName.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

const setCookie = (key, value, maxAge) => {
    if(maxAge) {
        document.cookie = `${key}=${value}; max-age=${maxAge}`;
    } else {
        document.cookie = `${key}=${value}`;
    }
};

const setTrueOrFalseValuesForCookie = (cookieName) => {
    if (getCookie(cookieName) === 'true') {
        setCookie(cookieName, 'false');
    } else if (getCookie(cookieName) === undefined) {
        setCookie(cookieName, 'true');
    }
};

const deleteCookie = (cookieName) => {
    let cookieDate = new Date();
    cookieDate.setTime (cookieDate.getTime() - 1 );
    document.cookie = `${cookieName}=; expires=${cookieDate.toUTCString()}`;
};



//Events
btnContactUs.addEventListener('click', () => {
    setCookie('experiment', 'novalue', 300);
    setTrueOrFalseValuesForCookie('new-user');
    console.log(document.cookie);
});



//Delete Cookie after end session
window.onbeforeunload = () => {
    deleteCookie('experiment');
    deleteCookie('new-user');
};