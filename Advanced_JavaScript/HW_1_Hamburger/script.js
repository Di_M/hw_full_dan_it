/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    if (arguments.length !== 2) {
        throw new HamburgerException('You entered wrong numbers arguments. Please enter 2 arguments(Size and Stuffing). ');
    } else if (!Hamburger.TYPE_SIZE.includes(size)) {
        throw new HamburgerException('You entered wrong size. Please enter Hamburger.SIZE_LARGE or Hamburger.SIZE_SMALL');
    } else if (!Hamburger.TYPE_STUFFING.includes(stuffing)) {
        throw new HamburgerException('You entered wrong stuffing. It has to Hamburger.STUFFING_CHEESE or Hamburger.STUFFING_SALAD, or Hamburger.STUFFING_POTATO');
    }

    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
}


/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {size: 'Small', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {size: 'Large', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {stuffing: 'Cheese', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {stuffing: 'Salad', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {stuffing: 'Potato', price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {topping: 'Mayo', price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {topping: 'Spice', price: 15, calories: 0};


Hamburger.TYPE_SIZE = [Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE];
Hamburger.TYPE_STUFFING = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD, Hamburger.STUFFING_POTATO];
Hamburger.TYPE_TOPPING = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE];

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param addedTopping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (addedTopping) {
    if (arguments.length !== 1) {
        throw new HamburgerException('You entered wrong numbers arguments. Please enter just one arguments(Topping)');
    }
    if (!Hamburger.TYPE_TOPPING.includes(addedTopping)) {
        throw new HamburgerException('You entered wrong topping. It has to be Hamburger.TOPPING_MAYO or Hamburger.TOPPING_SPICE');
    }
    this.topping.forEach(item => {
        if (item === addedTopping) {
            throw new HamburgerException(`You added topping: "${addedTopping.topping}" before. Please add another one.`);
        }
    });
    this.topping.push(addedTopping);
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param removedTopping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (removedTopping) {
    if (arguments.length !== 1) {
        throw new HamburgerException('You entered wrong numbers arguments');
    } else if (!Hamburger.TYPE_TOPPING.includes(removedTopping)) {
        throw new HamburgerException('You entered wrong topping. It has to be Hamburger.TOPPING_MAYO or Hamburger.TOPPING_SPICE');
    } else if (this.topping.length === 0) {
        throw new HamburgerException('Hamburger.topping is empty. Please add topping to Hamburger before removing');
    }

    if (this.topping.indexOf(removedTopping) !== -1) {
        this.topping.splice(this.topping.indexOf(removedTopping), 1);
    } else {
        throw new HamburgerException(`You try to remove Topping each has not been added. Please check you request.`);
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.topping.map(el => el.topping);
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing.stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let priceTopping = this.topping.reduce((sum, current) => {
        return sum + current.price;
    }, 0);
    return this.size.price + this.stuffing.price + priceTopping;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    let caloriesTopping = this.topping.reduce((sum, current) => {
        return sum + current.calories;
    }, 0);
    return this.size.calories + this.stuffing.calories + caloriesTopping;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
    this.name = 'Hamburger Exception';
    this.message = message;
}
HamburgerException.prototype = Error.prototype;


try {
    // маленький гамбургер с начинкой из сыра
    let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    // добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
    // сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
    // я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    // Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    // Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1
} catch (e) {
    console.log(e);
}



try {
    // не передали обязательные параметры
    // let h2 = new Hamburger(); // => HamburgerException: no size given

    // передаем некорректные значения, добавку вместо размера
    // let h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
    // => HamburgerException: invalid size 'TOPPING_SAUCE'

    // добавляем много добавок
    // let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    // h4.addTopping(Hamburger.TOPPING_MAYO);
    // h4.addTopping(Hamburger.TOPPING_MAYO);
    // HamburgerException: duplicate topping 'TOPPING_MAYO'
} catch (e) {
    console.log(e);
}


