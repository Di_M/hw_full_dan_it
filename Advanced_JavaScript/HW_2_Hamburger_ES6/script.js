class Hamburger {
    constructor(size, stuffing) {
        if (arguments.length !== 2) {
            throw new HamburgerException('You entered wrong numbers arguments. Please enter 2 arguments(Size and Stuffing)');
        } else if (!Hamburger.TYPE_SIZE.includes(size)) {
            throw new HamburgerException('You entered wrong size. Please enter Hamburger.SIZE_LARGE or Hamburger.SIZE_SMALL');
        } else if (!Hamburger.TYPE_STUFFING.includes(stuffing)) {
            throw new HamburgerException('You entered wrong stuffing. It has to Hamburger.STUFFING_CHEESE or Hamburger.STUFFING_SALAD, or Hamburger.STUFFING_POTATO');
        }

        this._size = size;
        this._stuffing = stuffing;
        this._topping = [];
    }

    addTopping(addedTopping) {
        if (arguments.length !== 1) {
            throw new HamburgerException('You entered wrong numbers arguments. Please enter just one arguments(Topping)');
        }
        if (!Hamburger.TYPE_TOPPING.includes(addedTopping)) {
            throw new HamburgerException('You entered wrong topping. It has to be Hamburger.TOPPING_MAYO or Hamburger.TOPPING_SPICE');
        }
        this._topping.forEach(item => {
            if (item === addedTopping) {
                throw new HamburgerException(`You added topping: "${addedTopping.topping}" before. Please add another one.`);
            }
        });
        this._topping.push(addedTopping);
    }

    removeTopping(removedTopping) {
        if (arguments.length !== 1) {
            throw new HamburgerException('You entered wrong numbers arguments');
        } else if (!Hamburger.TYPE_TOPPING.includes(removedTopping)) {
            throw new HamburgerException('You entered wrong topping. It has to be Hamburger.TOPPING_MAYO or Hamburger.TOPPING_SPICE');
        } else if (this._topping.length === 0) {
            throw new HamburgerException('Hamburger.topping is empty. Please add topping to Hamburger before removing');
        }

        if (this._topping.indexOf(removedTopping) !== -1) {
            this._topping.splice(this._topping.indexOf(removedTopping), 1);
        } else {
            throw new HamburgerException(`You try to remove Topping each has not been added. Please check you request.`);
        }
    }

    get Toppings() {
        return this._topping.map(el => el._topping);
    }

    get Size() {
        return this._size.size;
    }

    get Stuffing() {
        return this._stuffing.stuffing;
    }

    calculatePrice() {
        let priceTopping = this._topping.reduce((sum, current) => {
            return sum + current.price;
        }, 0);
        return this._size.price + this._stuffing.price + priceTopping;
    }

    calculateCalories() {
        let caloriesTopping = this._topping.reduce((sum, current) => {
            return sum + current.calories;
        }, 0);
        return this._size.calories + this._stuffing.calories + caloriesTopping;
    }

    static SIZE_SMALL = {size: 'Small', price: 50, calories: 20};
    static SIZE_LARGE = {size: 'Large', price: 100, calories: 40};
    static STUFFING_CHEESE = {stuffing: 'Cheese', price: 10, calories: 20};
    static STUFFING_SALAD = {stuffing: 'Salad', price: 20, calories: 5};
    static STUFFING_POTATO = {stuffing: 'Potato', price: 15, calories: 10};
    static TOPPING_MAYO = {topping: 'Mayo', price: 20, calories: 5};
    static TOPPING_SPICE = {topping: 'Spice', price: 15, calories: 0};


    static TYPE_SIZE = [Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE];
    static TYPE_STUFFING = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD, Hamburger.STUFFING_POTATO];
    static TYPE_TOPPING = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE];
}

class HamburgerException extends Error{
    constructor (message) {
        super();
        this.name = 'Hamburger Exception';
        this.message = message;
    }
}


try {
    // маленький гамбургер с начинкой из сыра
    let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    // добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // узнаем какая начинка добавлена
    console.log(`Topping has been added: ${hamburger.Stuffing}`);
    // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
    // сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
    // я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    // Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.Size === Hamburger.SIZE_LARGE); // -> false
    // Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.Toppings.length); // 1
} catch (e) {
    console.log(e);
}



try {
    // не передали обязательные параметры
    // let h2 = new Hamburger(); // => HamburgerException: no size given

    // передаем некорректные значения, добавку вместо размера
    // let h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
    // => HamburgerException: invalid size 'TOPPING_SAUCE'

    // добавляем много добавок
    // let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    // h4.addTopping(Hamburger.TOPPING_MAYO);
    // h4.addTopping(Hamburger.TOPPING_MAYO);
    // HamburgerException: duplicate topping 'TOPPING_MAYO'
} catch (e) {
    console.log(e);
}


