const headerMenu = document.getElementsByClassName('header__navigation-menu')[0];
const gamburger = document.getElementsByClassName('header__gamburger')[0];
const iconGamburger = document.getElementsByClassName('header__gamburger-icon')[0];

gamburger.addEventListener('click', function () {
    headerMenu.classList.toggle('header__navigation-menu--active');
    iconGamburger.classList.toggle('fa-times');
});
