$('a[href*=\\#]').on('click', function () {
    let elementClicked = $(this).attr('href');
    $('html, body').stop().animate({
        scrollTop: $(elementClicked).offset().top
    },2000);
});



$(window).on('scroll', function () {
    if($(this).scrollTop() > $(this).height()) {
        $('.btn_page-up').show();
    } else {
        $('.btn_page-up').hide();
    }
});



$('.btn_page-up').on('click', function () {
    $('html, body').stop().animate({
        scrollTop: 0
    },2000);
});



$('.btn_toggle-block').on('click', function() {
    $('.news__posts').slideToggle(2000, function () {
        if($('.news__posts').css('display') === 'none') {
            $('.btn_toggle-block').text('Show this block');
        } else {
            $('.btn_toggle-block').text('Hide this block');
        }
    });

});