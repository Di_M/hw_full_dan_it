
$('[data-tab]').click(function () {
    $(this).addClass('active').siblings('[data-tab]').removeClass('active');
    $(`[data-content = "${$(this).data('tab')}"]`).addClass('active').siblings('[data-content]').removeClass('active');
});