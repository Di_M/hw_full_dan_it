"use strict";

let buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].style.backgroundColor = '#33333a';
        if (event.key.toLowerCase() === buttons[i].innerText.toLowerCase()) {
            buttons[i].style.backgroundColor = '#0000ff';
        }
    }
});
