const btn = document.getElementsByTagName('button')[0];

btn.addEventListener('click', () => {
    btn.disabled = true;
    let inputCircleDiameter = document.createElement('input');
    let drawCircles = document.createElement('button');
    let containerCircles = document.createElement('div');

    inputCircleDiameter.value = 50;
    drawCircles.textContent = "Нарисовать";
    containerCircles.style.cssText = `display: flex;
                                      flex-wrap: wrap;
                                      width: ${inputCircleDiameter.value * 10}px;
                                      height: ${inputCircleDiameter.value * 10}px;`;

    document.body.append(inputCircleDiameter);
    document.body.append(drawCircles);
    document.body.append(containerCircles);

    drawCircles.addEventListener('click', () => {
        drawCircles.disabled = true;
        for (let i = 0; i < 100; i++) {
            let circle = document.createElement('div');
            let randomColor = [];

            for (let i = 0; i < 3; i++) {
                let val = Math.floor(Math.random() * 255);
                randomColor.push(val);
            }

            circle.style.cssText = `width: ${inputCircleDiameter.value}px; 
                                    height: ${inputCircleDiameter.value}px; 
                                    border-radius: 50%; 
                                    background-color: rgb(${randomColor.join(', ')})`;

            containerCircles.append(circle);
        }
    });

    inputCircleDiameter.addEventListener('input', () => {
        containerCircles.innerHTML = '';
        containerCircles.style.width = `${inputCircleDiameter.value * 10}px`;
        containerCircles.style.height = `${inputCircleDiameter.value * 10}px`;
        drawCircles.disabled = false;
    });

    containerCircles.onclick = function (event) {
        if (event.target !== this) {
            containerCircles.removeChild(event.target);
        }
    }
});