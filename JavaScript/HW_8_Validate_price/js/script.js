"use strict";

let inputPrice = document.getElementById('inputPrice');
let divPrice = document.querySelector('.price-container');
let inputValue = inputPrice.value;

let spanPrice = document.createElement('span');
spanPrice.classList.add('price-span');

let btnRemove = document.createElement('button');
btnRemove.classList.add('btn');
btnRemove.textContent = 'X';

let errorText = document.createElement('div');
errorText.textContent = 'Please enter correct price';
errorText.classList.add('error-text');

inputPrice.addEventListener('focus', () => {
    inputPrice.style.borderColor = 'green';
    inputPrice.style.color = 'black';
    if (divPrice.children[0] === spanPrice) {
        divPrice.removeChild(spanPrice);
    }
});

inputPrice.addEventListener('blur', () => {
    inputPrice.style.borderColor = 'grey';
    inputValue = inputPrice.value;

    if (inputValue === '' || parseInt(inputValue) <= 0 ) {
        inputPrice.style.color = 'red';
        divPrice.append(errorText);
    } else {

        inputPrice.style.color = 'green';
        spanPrice.textContent = `Текущая цена: ${inputValue}`;
        if (divPrice.children[2] === errorText) {
            divPrice.removeChild(errorText);
        }
        spanPrice.append(btnRemove);
        divPrice.prepend(spanPrice);
    }
});

btnRemove.addEventListener('click', () => {
    divPrice.removeChild(spanPrice);
    inputPrice.value = '';
});