"use strict";

const createNewUser = () => {
    let newUser = {
        set userFirstName(name) {
            this.firstName = name;
        },
        set userLastName(newVal) {
            this.lastName = newVal;
        },
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        }
    };

    Object.defineProperty(newUser, 'firstName', {
        value: newUser.userFirstName = setFirstName(),
        writable: false,
        configurable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        value: newUser.userLastName = setLastName(),
        writable: false,
        configurable: false
    });

    return newUser
};

const setFirstName = () => {
    let firstName = prompt('Please enter your first name', '');
    while (firstName === '' || firstName == null) {
        firstName = prompt('You did not enter your first name. Please try again', '');
    }
    return firstName
};

const setLastName = () => {
    let lastName = prompt('Please enter your last name', '');
    while (lastName === '' || lastName == null) {
        lastName = prompt('You did not enter your last name. Please try again', '');
    }
    return lastName
};

console.log(createNewUser().getLogin());
