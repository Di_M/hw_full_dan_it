// function point() {
//     this.is_mine = false;
//     this.mine_around = 0;
//     this.is_open = false;
// }
//
// let game = {
//     width: 10,
//     height: 10,
//     mine_count: 10,
//     open_count_mine: 10,
//     field: [],
//     fill_field: function () {
//         this.field =[];
//         for (let i = 0; i < this.width; i++) {
//             let tmp = [];
//             for (let j = 0; j < this.height; j++) {
//                  tmp.push(new point());
//             }
//             this.field.push(tmp);
//         }
//         for (let i = 0; i < this.mine_count;) {
//              let x = Math.floor(Math.random() * this.width);
//              let y = Math.floor(Math.random() * this.height);
//              if (!(this.field[x][y].is_mine)) {
//                 this.field[x][y].is_mine = true;
//                 i++;
//              }
//         }
//     },
//     mine_around_counter: function (x, y) {
//         let x_start = x > 0 ? x - 1 : x;
//         let y_start = y > 0 ? y - 1 : y;
//         let x_end = x < this.width - 1 ? x + 1 : x;
//         let y_end = y < this.height - 1 ? y + 1 : y;
//         let count = 0;
//         for (let i = x_start; i < x_end; i++) {
//             for (let j = y_start; j < y_end; j++) {
//                 if (this.field[i][y].is_mine && !(x === i && y === 1)) count++;
//             }
//         }
//         this.field[x][y].mine_around = count;
//     },
//     start_mine_counter: function () {
//         for (let i = 0; i < this.width; i++) {
//             for (let j = 0; j < this.height; j++) {
//                 this.mine_around_counter(i, j);
//             }
//         }
//     },
//     start: function () {
//         this.fill_field();
//         this.start_mine_counter();
//     }
// };
//
// window.onload = function () {
//   game.start();
//   console.log(game.field);
// };


//HTML
let divError = document.getElementsByClassName('game-over')[0];


//Ask size field
// let askNumberCell = +prompt('What field size for game "Minesweeper" do you prefer? \n Please, enter a number more than 5!', '');
//
// while (isNaN(askNumberCell) || askNumberCell == null || askNumberCell < 3 || (askNumberCell % 1 !== 0)) {
//     askNumberCell = +prompt('You entered wrong size. Please, try again.', askNumberCell);
// }
let askNumberCell = 10;


//Create Container
const container = document.createElement('div');
container.classList.add('container');



//Create Field
let createField = (sizeField) => {
    container.style.width = `${30 * sizeField}px`;
    container.style.height = `${30 * sizeField}px`;
    let amountMine = Math.floor(sizeField **2 / 6); // 6 mine

    // Position mines
    let mineArray = [];
    for (let i = 0; i < amountMine; i++) {
        let itemArray;
        let repeatNumber = 0;
        do {
            itemArray = Math.floor(Math.random() * sizeField **2);
            mineArray.forEach(i => {
                if (i === itemArray) repeatNumber = itemArray;
            });
        } while (repeatNumber === itemArray);
        mineArray.push(itemArray);
    }
    mineArray.sort((a, b) => a - b);


    // Add cells with mines
    let mineIndex = 0;
    for (let i = 1; i <= sizeField **2; i++) {
        let blockField = document.createElement('div');
        let minePosition = mineArray[mineIndex];
        if (i === minePosition) {
            blockField.classList.add('cell-mine');
            mineIndex++;
        }
        blockField.classList.add('cell', 'cell-closed');
        container.append(blockField);
    }

    // Count mines around
    console.log(mineArray);
    let countBomb = [];
    mineArray.forEach(i => {
        let x_left_top = i - sizeField - 1;
        let x_top = i - sizeField;
        let x_right_top = i - sizeField + 1;
        let x_left = i - 1;
        let x_right = i + 1;
        let x_left_bottom = i + sizeField - 1;
        let x_bottom = i + sizeField;
        let x_right_bottom = i + sizeField + 1;
        if (x_left_top > 0 && (x_left_top % sizeField) !== 0) countBomb.push(x_left_top);
        if (x_top > 0) countBomb.push(x_top);
        if (x_right_top > 0 && ((x_right_top - 1) % sizeField) !== 0) countBomb.push(x_right_top);
        if (x_left > 0 && (x_left % sizeField) !== 0) countBomb.push(x_left);
        if (((x_right - 1) % sizeField) !== 0) countBomb.push(x_right);
        if ((x_left_bottom < sizeField**2) && (x_left_bottom % sizeField) !== 0) countBomb.push(x_left_bottom);
        if (x_bottom < sizeField**2) countBomb.push(x_bottom);
        if (x_right_bottom < sizeField**2 && ((x_right_bottom - 1) % sizeField) !== 0) countBomb.push(x_right_bottom);
    });
    countBomb.sort((a, b) => a - b);
    console.log(countBomb);

    // for (let j = 0; j <= countBomb.length; j++) {
    //     if (j === countBomb[j]) {
    //         [...container.children][j].innerHTML += 1;
    //     }
    // }

    document.body.append(container);
};

createField(askNumberCell);


container.addEventListener('click', (event) => {
    if (event.target.classList.contains('cell-mine')) {
        [...container.children].forEach(item => {
            if(item.classList.contains('cell-mine')) {
                item.classList.remove('cell-closed');
            }
        });
        divError.style.visibility = "visible";
        container.style.pointerEvents = "none";
    } else if (!(event.target.classList.contains('cell-flag'))) {
        event.target.classList.add('cell-checked');
    }
});


container.addEventListener('contextmenu', (event) => {
    event.preventDefault();
    if (!event.target.classList.contains('cell-checked')) {
        event.target.classList.toggle('cell-flag');
        event.target.classList.toggle('cell-closed');
    }
});
