let userNumber = +prompt('Please enter some number', '');
while (userNumber === '' || isNaN(userNumber) || userNumber == null || (userNumber % 1 !== 0)) {
    userNumber = +prompt('You did not enter a correct number. Please try again', userNumber);
}

const getFactorial = (n) => {
    return (n !== 1) ? n * getFactorial(n - 1) : 1;
};

alert(getFactorial(userNumber));
