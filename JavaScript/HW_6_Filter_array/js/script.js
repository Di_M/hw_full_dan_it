"use strict";

let array1 = ['hello', 'world', 23, '23', null];
let type = 'string';

// const filterBy = (someArray, someType) => {
//     let newArray = [];
//     someArray.forEach( item => {
//         if (typeof(item) !== someType) {
//             newArray.push(item);
//         }
//     });
//     return newArray
// };

const filterBy = (someArray, someType) => someArray.filter(item => typeof(item) !== someType);


console.log(filterBy(array1, type));
