
// // get numbers with validation
const getNumbers = () => {
    let numbers = [];

    for (let i = 0; i < 2; i++) {
        numbers[i] = prompt('Please enter a number', '');

        while (numbers[i] === '' || isNaN(numbers[i]) || numbers[i] == null) {
            numbers[i] = prompt('You entered not a number. Please enter correct number', numbers[i]);
        }
    }
    return numbers;
};

// get math operation
const getOperation = () => {
    let mathOperation = prompt('Please enter math operation ("+", "-", "*", "/")', '+');
    while (mathOperation !== '+' && mathOperation !== '-' && mathOperation !== '*' && mathOperation !== '/') {
        mathOperation = prompt('You entered wrong operation. Please enter operation like "+", "-", "*", "/")', '+');
    }
    return mathOperation;
};

// final result
const showResult = (dataNumbers, operation) => {
    let result = dataNumbers;
    result.splice(1, 0, operation);
    return eval(result.join(' '));
};

console.log(showResult(getNumbers(),getOperation()));