let userNumber = +prompt('Please enter some number', '');
while (userNumber === '' || isNaN(userNumber) || userNumber == null || userNumber % 1 !== 0 || userNumber === 0) {
    userNumber = +prompt('You did not enter a correct number. Please try again', userNumber);
}

const getFibonachi = (f0, f1, n) => {
    if (n < 1) {
        for (let i = -1; i >= n; i--) {
            let f2 = f0 - f1;
            if(f1 < 0) {
                f2 = f0 + f1;
            }
            f1 = f0;
            f0 = f2;
        }
        return f0
    }
    else if(n === 1) {
        return f0
    } else if(n === 2) {
        return f1
    } else {
        for (let i = 3; i <= n; i++) {
            let f2 = f0 + f1;
            f0 = f1;
            f1 = f2;
        }
        return f1
    }
};

alert(getFibonachi(1, 1, userNumber));