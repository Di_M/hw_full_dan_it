//HTML
const head = document.head;
const link = document.createElement('link');
link.setAttribute('rel', 'stylesheet');


//Helpers
const checkThemeInStorage = () => {
    if (localStorage.getItem('theme') === 'style_dark') {
        link.href = 'css/style_dark.css';
    } else {
        link.href = 'css/style.css';
    }

    head.append(link);
};

checkThemeInStorage();


//Events
window.addEventListener('load', () => {
    const btnChangeTheme = document.querySelector('.btn-change-theme');
    btnChangeTheme.addEventListener('click', () => {
        if (localStorage.getItem('theme') === 'style_dark') {
            link.href = 'css/style.css';
            localStorage.removeItem('theme');
        } else {
            link.href = 'css/style_dark.css';
            localStorage.setItem('theme', 'style_dark');
        }
    });
});








