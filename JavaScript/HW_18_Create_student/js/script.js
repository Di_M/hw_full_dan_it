// Object student
const student = {
    name: null,
    "last name": null,
    createTabel() {
        return this.tabel = {
            setSublectAndMark() {
                let subject,
                    mark;
                do {
                    subject = prompt('Please enter your subject', '');
                    while (subject === '') {
                    subject = prompt('You entered empty subject. Please try again', '');
                    }

                    if (subject === null) {
                        break
                    } else {
                        mark = +prompt('Please enter your mark for ' + subject, '');
                        while (mark === '' || mark == null || isNaN(mark)) {
                            mark = +prompt('You entered empty or not a number mark. Please try again', '');
                        }
                        student.tabel[subject] = mark;
                    }
                } while(subject != null);
            },
        }
    },
    countBadMarks() {
        let countBadMarks = 0;
        for (let i in this.tabel) {
            if (this.tabel[i] < 4) {
                countBadMarks++
            }
        }
        if (countBadMarks === 0) {
            alert('Студент переведен на следующий курс!');
        } else {
            alert(`У студента плохих оценок: ${countBadMarks}.`);
        }
    },
    averageMark() {
        let sumMark = 0,
            countMarks = 0;

        for (let i in this.tabel) {
            sumMark += this.tabel[i];
            countMarks++;
        }

        if (sumMark / countMarks > 7 ) {
            alert('Студенту назначена стипендия!');
        } else {
            alert('Студент без стипендии! Нужно учиться лучше');
        }
    }
};



//Function Ask Name and Last name
const setName = () => {
    let firstName = prompt('Please enter your first name', '');
    while (firstName === '' || firstName == null) {
        firstName = prompt('You did not enter your first name. Please try again', '');
    }
    return firstName
};

const setLastName = () => {
    let lastName = prompt('Please enter your last name', '');
    while (lastName === '' || lastName == null) {
        lastName = prompt('You did not enter your last name. Please try again', '');
    }
    return lastName
};



//Set the Name and Last name
student.name = setName();
student["last name"] = setLastName();



//Сall methods
student.createTabel().setSublectAndMark();
student.countBadMarks();
student.averageMark();