"use strict";

let inputPass = document.querySelectorAll('input[type="password"]');
let iconPass = document.querySelectorAll('.fas');
let btn = document.querySelector('.btn');
let divError = document.querySelector('.error-text');

for (let i = 0; i < iconPass.length; i++) {
    iconPass[i].addEventListener('click', (event) => {
        event.target.classList.toggle('fa-eye-slash');
        if (inputPass[i].type === 'password') {
            inputPass[i].type = 'text';
        } else {
            inputPass[i].type = 'password';
        }
    });
}

btn.addEventListener('click', (event) => {
    if (inputPass[0].value === inputPass[1].value) {
        divError.style.display = 'none';
        alert("You are welcome!");
    } else {
        divError.style.display = 'block';
    }
    event.preventDefault();
});