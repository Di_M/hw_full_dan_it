"use strict";

const createNewUser = () => {
    let newUser = {
        set userFirstName(name) {
            this.firstName = name;
        },
        set userLastName(newVal) {
            this.lastName = newVal;
        },
        set birthdayUser(birthd) {
            this.birthday = birthd;
        },
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        getAge() {
            let dayOfBirthday = Number(this.birthday.substring(0,2));
            let monthOfBirthday = Number(this.birthday.substring(3,5));
            let yearOfBirthday = Number(this.birthday.substring(6,10));
            let birthdayDate = new Date(yearOfBirthday, monthOfBirthday-1, dayOfBirthday);
            let nowDate = new Date();
            let age = nowDate.getFullYear() - birthdayDate.getFullYear();
            let monthAge = nowDate.getMonth() - birthdayDate.getMonth();
            if (monthAge < 0 || (monthAge === 0 && nowDate.getDate() < birthdayDate.getDate())) {
                age = age - 1;
            }
            return age
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6,10)
        }
    };

    Object.defineProperty(newUser, 'firstName', {
        value: newUser.userFirstName = setFirstName(),
        writable: false,
        configurable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        value: newUser.userLastName = setLastName(),
        writable: false,
        configurable: false
    });

    newUser.birthdayUser = setBirthdayUser();
    return newUser
};

const setFirstName = () => {
    let firstName = prompt('Please enter your first name', '');
    while (firstName === '' || firstName == null) {
        firstName = prompt('You did not enter your first name. Please try again', '');
    }
    return firstName
};

const setLastName = () => {
    let lastName = prompt('Please enter your last name', '');
    while (lastName === '' || lastName == null) {
        lastName = prompt('You did not enter your last name. Please try again', '');
    }
    return lastName
};

const setBirthdayUser = () => {
    let birthdayUser = prompt('Please enter your birthday in format "dd.mm.yyyy"', '');
    while (birthdayUser === '' || birthdayUser == null) {
        birthdayUser = prompt('You did not enter your first name. Please try again', '');
    }
    return birthdayUser
};

// console.log(createNewUser().getLogin());
console.log(createNewUser().getAge());
console.log(createNewUser().getPassword());